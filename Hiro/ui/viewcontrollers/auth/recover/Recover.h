//
//  Recover.h
//  Hiro
//
//  Created by George Vashakidze on 12/24/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Base.h"

@interface Recover : Base
@property (weak, nonatomic) IBOutlet UITextField *txEmail;

@end
