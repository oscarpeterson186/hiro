//
//  Recover.m
//  Hiro
//
//  Created by George Vashakidze on 12/24/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Recover.h"

@implementation Recover{
    NSString *accessToken;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


-(void)loadDefaults{
    self.view.backgroundColor = colorFromARGBString(Colors.BACKGROUND_COLOR);
    setTextInputStartPosition(_txEmail);
}

- (IBAction)sendButtonClicked:(id)sender {
    [self send];
}
- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) send{
    if([_txEmail.text length] == 0) return;
    [self requestAccessToken];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == _txEmail){
        [self send];
    }
    return YES;
}

-(void) didGetAccessTokenForRegisterSucceed :(NSDictionary *) accessTokenObj{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        NSString *token = [accessTokenObj objectForKey:Models.User.ACCESS_TOKEN];
        if(accessTokenObj == nil || token == nil){
            [super hideLoader];
            launchModal(self, ErrorCodes.ERROR, ErrorCodes.YOUR_PROCESS_COULD_NOT_BE_COMPLETED,DARK, FALSE);
            return;
        }
        accessToken = token;
        [[self httpWorker] resetPassword:_txEmail.text :accessToken];
    });
}

-(void) didPasswordResetSucceed :(NSDictionary *) userData{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [super hideLoader];
        if(userData == nil || userData.count == 0){
            launchModal(self, ErrorCodes.ERROR, ErrorCodes.YOUR_PROCESS_COULD_NOT_BE_COMPLETED,DARK, FALSE);
            return;
        }
        //TODO: show success page here!!!
    });
}

@end
