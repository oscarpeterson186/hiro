//
//  Login.h
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Base.h"

@interface Login : Base
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UILabel *txLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@property (weak, nonatomic) IBOutlet UIView *loginView;

@end
