//
//  Login.m
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Login.h"
#import "Register.h"
#import "Recover.h"
#import "Categories.h"
#import "CategoryChooser.h"
#import "User.h"

@implementation Login
{
    UIStoryboard* storyboard;
}
- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)loadDefaults{
    _loginView.backgroundColor = colorFromARGBString(Colors.BACKGROUND_COLOR);
    setTextInputStartPosition(_txtPassword);
    setTextInputStartPosition(_txtUserName);
    setButtonTextUnderlineStyle(_btnForgotPassword);
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField == _txtUserName){
        [_txtUserName endEditing:YES];
        [_txtPassword becomeFirstResponder];
    }else if(textField == _txtPassword){
        [self hideKeabord];
        if(![self validateInputs])
            return NO;
        [self makeLogin];
    }
    return YES;
}


-(void) makeLogin{
    [self.view endEditing:YES];
    NSString * userName = self.txtUserName.text;
    NSString * password = self.txtPassword.text;
    if([self validateInputs])
    {
        [super showLoader];
        [self.httpWorker login:userName andPassword:password];
    }else{
        launchModal(self, ErrorCodes.ERROR, ErrorCodes.FILL_OUT_ALL_FIELDS, DARK, FALSE);
    }
}

-(BOOL) validateInputs{
    return  (![self.txtUserName.text isEqualToString:@""] &&
             ![self.txtPassword.text isEqualToString:@""]);
}

#pragma mark - actions:
- (IBAction)btnLoginButtonClicked:(UIButton *)sender {
    [self makeLogin];
}

- (IBAction)btnForgotPasswordClicked:(id)sender {
    Recover * recoverController = [storyboard instantiateViewControllerWithIdentifier:@"RecoverViewController"];
    [self.navigationController pushViewController:recoverController animated:YES];
}

- (IBAction)btnSignUpClicked:(id)sender {
    Register * regController = [storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:regController animated:YES];
}

-(void) launchMainScreen{
    //HERE check if current user already has chosen his/her categories.
    //if true then open causes page otherwise open category chooser.
    
    CategoryChooser * vc = [storyboard instantiateViewControllerWithIdentifier:@"CategoryChooserViewController"];
    [self presentViewController:vc animated:YES completion:nil];

}

-(void) didLoginSucceed :(NSDictionary *) userData {
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        if (userData != nil || userData.count > 0) {
            [User instance].currentUser = userData;
            NSDictionary *categories = [userData objectForKey:Models.User.CATEGORIES];
            if (categories != nil && categories.count > 0) {
                [super hideLoader];
                [self launchMainScreen];
            }else{
                [self.httpWorker listOfcategories :[[User instance] getUserAccessToken]];
            }
        }else{
            [super hideLoader];
            NSString * title = ErrorCodes.ERROR;
            NSString * alertText = ErrorCodes.USERNAME_OR_PASSWORD_INCORRECT;
            launchModal(self, title, alertText,DARK, FALSE);
        }
        
    });
}

-(void) didGettingCategoriesSucceed :(NSDictionary *) categories{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [super hideLoader];
        if(categories != nil && categories.count > 0){
            [Categories instance].categories = [categories objectForKey:Models.User.DATA];
            [self launchMainScreen];
        }else{
            NSString * title = ErrorCodes.ERROR;
            NSString * alertText = ErrorCodes.COULD_NOT_FETCH_CATEGORIES_FROM_SERVER;
            launchModal(self, title, alertText,DARK, FALSE);
        }
    });
}


@end
