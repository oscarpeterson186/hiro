//
//  Register.h
//  Hiro
//
//  Created by George Vashakidze on 12/16/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Base.h"

@interface Register : Base
@property (weak, nonatomic) IBOutlet UIButton *btnBackToLogin;
@property (weak, nonatomic) IBOutlet UITextField *txEmail;
@property (weak, nonatomic) IBOutlet UIView *registerView;
@property (weak, nonatomic) IBOutlet UIView *viewTerms;
@property (weak, nonatomic) IBOutlet UITextView *tvTerms;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cstrBackToLogin;
@property (weak, nonatomic) IBOutlet UIView *viewImagesContainer;
@property (weak, nonatomic) IBOutlet UIImageView *chTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnActivateAccount;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *stepsImageContainer;
@property (weak, nonatomic) IBOutlet UIView *viewTextViewContainer;
@end
