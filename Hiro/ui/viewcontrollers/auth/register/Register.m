//
//  Register.m
//  Hiro
//
//  Created by George Vashakidze on 12/16/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Register.h"

@implementation Register {
    int index;
    NSArray *placeHolders;
    NSMutableArray *values;
    int cstrBackToLoginDefaultHeight;
    NSString *backTo;
    NSString *selectedImagePath;
    NSString *emptyImagePath;
    BOOL checked;
    dispatch_semaphore_t waiter;
    BOOL OPERATION_SHOULD_BREAK;
    NSString * accessToken;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)loadDefaults{
    _registerView.backgroundColor = colorFromARGBString(Colors.BACKGROUND_COLOR);
    setTextInputStartPosition(_txEmail);
    setButtonTextUnderlineStyle(_btnBackToLogin);
    _viewTextViewContainer.layer.contents = (id)[UIImage imageNamed:@"TermsInput"].CGImage;
    placeHolders = @[@"Name",@"Username", @"Email", @"Password"];
    values = [[NSMutableArray alloc] init];
    _txEmail.placeholder = placeHolders[0];
    cstrBackToLoginDefaultHeight = _cstrBackToLogin.constant;
    backTo = @"Back to ";
    selectedImagePath = @"stagecurrent";
    emptyImagePath = @"stagenext";
    [self registerTermsCheckBoxTap];
    waiter = dispatch_semaphore_create(0);
}

-(void)registerTermsCheckBoxTap {
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnAgreementBoxDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_chTerms setUserInteractionEnabled:YES];
    [_chTerms addGestureRecognizer:singleTap];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    OPERATION_SHOULD_BREAK = NO;
    if(index == 3){
        values[3] = _txEmail.text;
        [self hideKeabord];
        [self loadRoundButtons];
        [self loadTerms];
        [self secureTextBox];
        return YES;
    }else if(index < 3){
        [self loadForm];
        if(_txEmail.text.length == 0)return NO;
        values[index] = _txEmail.text;
        [self setBackButtonText :placeHolders[index]];
        
        /*
         Hang up main thread and let's wait async task to be finished.
         Once done check, if user exists or email ar valid and then continue to last screen
         */
        [self getUserAccessTokenOrEmail:^(BOOL result){
            NSLog(@"Processing...");
        }];
        dispatch_semaphore_wait(waiter, DISPATCH_TIME_FOREVER);
        if(OPERATION_SHOULD_BREAK)return NO;
        
        
        index++;
        _txEmail.placeholder = placeHolders[index];
        _txEmail.text = @"";
        [self secureTextBox];
        [self loadRoundButtons];
    }
    return YES;
}



/*if called twice, than the second call was for email*/
-(void)requestUserCheck :(NSString *) token{
    accessToken = token;
    [self.httpWorker checkIfUserExists:_txEmail.text :token];
}

-(void)getUserAccessTokenOrEmail:(void(^)(BOOL result)) completion {
    /*Getting access token*/
    if(index == 1){
        completion(YES);
        [self requestAccessToken];
    }
    /*Cheking user email*/
    else if (index == 2){
        [super showLoader];
        completion(YES);
        [self requestUserCheck:accessToken];
    }else{
        completion(NO);
        dispatch_semaphore_signal(waiter);
    }
    
}

-(void)loadRoundButtons{
    int i = 0;
    while(i <= index){
        [self setImageByIndex:i :selectedImagePath];
        i++;
    }
    while(i < 4){
        [self setImageByIndex:i :emptyImagePath];
        i++;
    }
}

-(void)loadTerms{
    _txEmail.hidden = true;
    _viewTerms.hidden = false;
    _cstrBackToLogin.constant = _viewTerms.frame.size.height - 30;
    [self setBackButtonText :@"Login"];
    [self setImageByIndex:4 :selectedImagePath];
    index = 0;
}

-(void) setBackButtonText :(NSString *)title{
    setButtonTextUnderlineStyleWithTitle(_btnBackToLogin, [backTo stringByAppendingString:title]);
}

-(void) setImageByIndex :(int) inx :(NSString *)title{
    ((UIImageView *)_stepsImageContainer[inx]).image = [UIImage imageNamed:title];
}

-(void)loadForm{
    _txEmail.hidden = false;
    _viewTerms.hidden = true;
    _cstrBackToLogin.constant = cstrBackToLoginDefaultHeight;
}

- (IBAction)btnBackButtonPressed:(id)sender {
    index--;
    if(index > -1){
        [self loadRoundButtons];
        if(index-1 < 0)
            [self setBackButtonText :@"Login"];
        else
            [self setBackButtonText :placeHolders[index-1]];
        _txEmail.placeholder = placeHolders[index];
        _txEmail.text = values[index];
    }
    [self secureTextBox];
    if(index == -1){
         [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)secureTextBox{
    if(index != 3){
        _txEmail.secureTextEntry = NO;
    }else{
        _txEmail.secureTextEntry = YES;
    }
}

-(void) tapOnAgreementBoxDetected{
    NSString * termsImagePath = @"Terms_CheckBox";
    if(!checked){
        termsImagePath = @"Terms_CheckBox_Active";
    }
    checked = !checked;
    _btnActivateAccount.enabled = checked;
    _chTerms.image = [UIImage imageNamed:termsImagePath];
}

- (IBAction)makeRegister:(id)sender {
    [super showLoader];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:4];
    [params setObject:values[0] forKey:Models.User.NAME];
    [params setObject:values[1] forKey:Models.User.USERNAME];
    [params setObject:values[2] forKey:Models.User.EMAIL];
    [params setObject:values[3] forKey:Models.User.PASSWORD];
    [self.httpWorker registerUser:params :[Models.User.BEANER_AUTH_HEADER_KEY stringByAppendingString:accessToken]];
}

-(void) didGetAccessTokenForRegisterSucceed :(NSDictionary *) accessTokenObj{
    NSString *token = [accessTokenObj objectForKey:Models.User.ACCESS_TOKEN];
    if(accessTokenObj == nil || token == nil){
        OPERATION_SHOULD_BREAK = YES;
    }
    else{
        [self requestUserCheck :token];
    }
}

-(void)didCheckIfUserExistsSucceed:(NSDictionary *)userData{
    if(userData != nil && userData.count > 0)
    {
        OPERATION_SHOULD_BREAK = YES;
    }
    dispatch_semaphore_signal(waiter);
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [super hideLoader];
        if(OPERATION_SHOULD_BREAK){
            NSString * title = ErrorCodes.ERROR;
            NSString * alertText = @"";
            if(index == 1){
                alertText = ErrorCodes.USERNAME_ALREADY_EXISTS;
            }else if(index == 2){
                alertText = ErrorCodes.EMAIL_ALREADY_EXISTS;
            }
            launchModal(self, title, alertText,DARK, FALSE);
        }
    });
}

-(BOOL)didRegisterSucceed:(BOOL)status{
    dispatch_async(dispatch_get_main_queue(), ^(void) {
        [super hideLoader];
        NSString * title = ErrorCodes.ERROR;
        NSString * alertText = ErrorCodes.USERNAME_OR_PASSWORD_INCORRECT;
        if (status) {
            //TODO needs to be removed!!!
            title = Successes.SUCCESS;
            alertText = @"Register was succesfull";
        }
        [self.navigationController popViewControllerAnimated:YES];
        launchModal(self, title, alertText,DARK, FALSE);
    });
    return status;
}




@end
