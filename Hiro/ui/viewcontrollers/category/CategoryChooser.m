//
//  CategoryChooser.m
//  Hiro
//
//  Created by George Vashakidze on 12/21/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "CategoryChooser.h"
#import "Categories.h"

@interface CategoryChooser ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCategoryContainerHeight;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *heightCollection;
@property (strong, nonatomic) IBOutletCollection(NSLayoutConstraint) NSArray *widthCollection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constBtnDoneBotton;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnCategoryCollection;
@property (weak, nonatomic) IBOutlet UITextView *tvMaximum;
@property (weak, nonatomic) IBOutlet UILabel *lbSelectYourInterest;
@property (weak, nonatomic) IBOutlet UIView *vBottomCategoryContainer;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lblCategoryContainer;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *chosenCategoriesColletion;


@end

@implementation CategoryChooser{
    long centerPointX;
    long centerPointY;
    long origWidth;
    long origHeight;
    CGPoint centerPosition;
    NSMutableArray *displayedSubCategories;
    NSMutableArray *chosenSubCategories;
    long chosenSubCategoriesCount;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if(IS_IPHONE_4 || IS_IPHONE_5)
        [self adjustConstraints];
}

-(void)adjustConstraints{
    int catContHeight = 50;
    int catItemHeight = 88;
    int catItemWidth = 75;
    if(IS_IPHONE_4){
        catItemHeight -= 15;
        catItemWidth -= 15;
        _constBtnDoneBotton.constant -= 20;
    }
    _constCategoryContainerHeight.constant = catContHeight;
    for (int i=0; i<_heightCollection.count; i++) {
        ((NSLayoutConstraint*)_heightCollection[i]).constant = catItemHeight;
        ((NSLayoutConstraint*)_widthCollection[i]).constant = catItemWidth;
    }
}

-(void)loadDefaults{
    _vCategory.backgroundColor = colorFromARGBString(Colors.BACKGROUND_COLOR);
    centerPointY = [_btnCategoryCollection[1] center].y;
    centerPointX = [_btnCategoryCollection[0] center].x;
    origWidth    = [_btnCategoryCollection[0] frame].size.width;
    origHeight   = [_btnCategoryCollection[0] frame].size.height;
    chosenSubCategories = [[NSMutableArray alloc] initWithCapacity:6];
    chosenSubCategoriesCount = 0;
}

-(void)makeSelectedByIndex :(long)index{
    [self clearCurrentSubCategories];
    BOOL state = ![_btnCategoryCollection[index] isSelected];
    for(int i = 0; i < _btnCategoryCollection.count; i++)
        [_btnCategoryCollection[i] setSelected:NO];
    [_btnCategoryCollection[index] setSelected:state];
}

-(void) moveToCenter:(UIButton *) selected{
    [self disableButtonTemporally:NO];
    [self removeConstraintsFromCategories];
    [UIView animateWithDuration:1.0 animations:^{
        [self moveUpCategoriesToTop:selected];
        selected.center = CGPointMake(centerPointX, centerPointY);
    } completion:^(BOOL finished) {
        [self loadSubCategories:selected];
        [self disableButtonTemporally:YES];
        [selected setUserInteractionEnabled:NO];
    }];
}

-(void) removeConstraintsFromCategories {
    for(int i = 0; i < _btnCategoryCollection.count; i++){
        [_btnCategoryCollection[i] setTranslatesAutoresizingMaskIntoConstraints:YES];
    }
    _vBottomCategoryContainer.translatesAutoresizingMaskIntoConstraints = YES;
    _tvMaximum.hidden = true;
    _lbSelectYourInterest.hidden = true;
}

-(void) disableButtonTemporally:(BOOL) ok{
    for(int i = 0; i < _btnCategoryCollection.count; i++){
        [_btnCategoryCollection[i] setUserInteractionEnabled:ok];
    }
}

-(void) moveUpCategoriesToTop:(UIButton *) button{
    long topSize = _lbSelectYourInterest.frame.size.width / 3;
    long padding = topSize - origWidth;
    long startPosition = _lbSelectYourInterest.frame.origin.x * 2 + padding;
    for(int i = 0; i < _btnCategoryCollection.count; i++){
        if(i != button.tag){
            CGRect buttonNewSize = CGRectMake(startPosition, _lbSelectYourInterest.frame.origin.y, origWidth / 2.5, origHeight / 2.5);
            [_btnCategoryCollection[i] setFrame:buttonNewSize];
            startPosition += padding + origWidth;
            [_lblCategoryContainer[i] setHidden:YES];
        }else{
            [_btnCategoryCollection[i] setCenter:CGPointMake(centerPointX, centerPointY)];
            [_btnCategoryCollection[i] setSize:CGSizeMake(origWidth, origHeight)];
            [_btnCategoryCollection[i] setCenter:CGPointMake(centerPointX, centerPointY)];
            [_btnCategoryCollection[i] setSize:CGSizeMake(origWidth, origHeight)];
            [_lblCategoryContainer[i] setHidden:NO];
        }
    }
}

-(void) clearCurrentSubCategories{
    for (UIButton *item in displayedSubCategories) {
        [item removeFromSuperview];
    }
}

-(void)loadSubCategories:(UIButton *) selected{
    NSArray *selectedCategoryItems = [self selectedCategory: selected.tag];
    displayedSubCategories = [[NSMutableArray alloc] init];
    float step = (float)(2 * M_PI) / selectedCategoryItems.count;
    float alfa = 0;
    for(int i = 0; i < selectedCategoryItems.count; i++){
        int x = selected.center.x + (origWidth + 40) * sinf(alfa);
        int y = selected.center.y + (origWidth + 40) * cosf(alfa);
        UIButton *iv = [[UIButton alloc] init];
        iv.center = CGPointMake(x,y);
        iv.frame = CGRectMake(x-24, y-29, 48, 58);
        NSString * imageName = [NSString stringWithFormat:@"%@%@",selectedCategoryItems[i],SubCategoryStates.BIG];
        NSString * imageNameACtive = [NSString stringWithFormat:@"%@%@",selectedCategoryItems[i],SubCategoryStates.BIG_ACTIVE];
        [iv setUserInteractionEnabled:YES];
        [iv setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [iv setBackgroundImage:[UIImage imageNamed:imageNameACtive] forState:UIControlStateSelected];
        [iv setBackgroundImage:[UIImage imageNamed:imageNameACtive] forState:UIControlStateFocused | UIControlStateHighlighted];
        [iv setBackgroundImage:[UIImage imageNamed:imageNameACtive] forState:UIControlStateSelected | UIControlStateHighlighted];
        iv.transform = CGAffineTransformMakeScale(0, 0);
        [iv setTitle:selectedCategoryItems[i] forState:UIControlStateNormal];
        iv.titleLabel.layer.opacity = 0.0;
        
        if ([self checkIfAlreadyExists:selectedCategoryItems[i]]) {
            [iv setSelected:YES];
        }
        
        [iv addTarget:self
               action:@selector(subCategoryClicked:)
     forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:iv];
        [displayedSubCategories addObject:iv];
        [UIView animateWithDuration:1 delay:i * 0.16 usingSpringWithDamping:0.6 initialSpringVelocity:0.6 options:UIViewAnimationOptionTransitionNone animations:^{
            iv.transform = CGAffineTransformMakeScale(1, 1);
        } completion:nil];
        alfa += step;
    }
}

-(BOOL) checkIfAlreadyExists :(NSString *)imgName{
    for (NSString * item in chosenSubCategories) {
        if ([item isEqualToString:imgName]) {
            return true;
        }
    }
    return false;
}

-(void) subCategoryClicked :(UIButton *) sender{
    if(chosenSubCategoriesCount == 6l) return;
    NSString *title = sender.titleLabel.text;
    if([self checkIfAlreadyExists:title])return;
    [sender setSelected:!sender.selected];
    BOOL textSet = NO;
    int clickedPosition = -1;
    if (chosenSubCategoriesCount < 6){
        for (int i = 0; i < chosenSubCategories.count; i++) {
            if([chosenSubCategories[i] length] == 0){
                chosenSubCategories[i] = title;
                textSet = YES;
                chosenSubCategoriesCount++;
                clickedPosition = i;
                break;
            }
        }
        if (!textSet) {
            [chosenSubCategories addObject:title];
            chosenSubCategoriesCount++;
        }
    }
    
    NSString * imageName = [NSString stringWithFormat:@"%@%@",title,SubCategoryStates.MINI];
    NSString * imageNameACtive = [NSString stringWithFormat:@"%@%@",title,SubCategoryStates.MINI_CLICKED];
    long t = clickedPosition > -1 ? clickedPosition : chosenSubCategories.count - 1;
    UIButton * selectedImage = (UIButton *)_chosenCategoriesColletion[t];
    selectedImage.tag = t;
    selectedImage.titleLabel.text = title;
    selectedImage.titleLabel.layer.opacity = 0.0;
    [selectedImage setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [selectedImage setBackgroundImage:[UIImage imageNamed:imageNameACtive] forState:UIControlStateHighlighted];
    [selectedImage setUserInteractionEnabled:YES];
    [selectedImage addTarget:self
                      action:@selector(subCategoryMiniClicked:)
            forControlEvents:UIControlEventTouchUpInside];
    [_btnDone setEnabled:YES];
}

-(void) subCategoryMiniClicked :(UIButton *) sender{
    if(chosenSubCategoriesCount == 0 ||
       [sender.titleLabel.text length] == 0
       )return;
    sender.highlighted = !sender.highlighted;
    
    chosenSubCategories[sender.tag] = @"";
    UIButton * clickedImage = (UIButton *)_chosenCategoriesColletion[sender.tag];
    [clickedImage setBackgroundImage: [UIImage imageNamed:@"EmptySpace"] forState:UIControlStateNormal];
    [clickedImage setBackgroundImage: [UIImage imageNamed:@"EmptySpace"] forState:UIControlStateHighlighted];
        chosenSubCategoriesCount--;
    for (UIButton *item in displayedSubCategories) {
        if ([item.titleLabel.text isEqualToString:sender.titleLabel.text]) {
            [item setSelected:NO];
        }
    }
    sender.titleLabel.text = @"";
    if (chosenSubCategoriesCount == 0) {
        [_btnDone setEnabled:NO];
    }
}

-(NSArray *) parseCategory :(NSString *) categoryName{
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:15];
    NSDictionary *categories = [Categories instance].categories;
    for (NSDictionary *item in categories) {
        NSDictionary *category = [item objectForKey:Models.User.CATEGORY];
        NSDictionary *parentCategory = [category objectForKey:Models.User.PARENT_CATEGORY];
        if(parentCategory != nil && [parentCategory class] != [NSNull class]){
            NSDictionary *innerCategory = [parentCategory objectForKey:Models.User.CATEGORY];
            if(innerCategory != nil){
                if ([[innerCategory objectForKey:Models.User.NAME] isEqualToString:categoryName]) {
                    [images addObject:[category objectForKey:Models.User.SLUG]];
                }
            }
        }
    }
    return images;
}

-(NSArray *) selectedCategory :(long) tag{
    NSArray *subCategories;
    switch (tag) {
        case 0:
            subCategories = [self parseCategory:@"PLANET"];
            break;
        case 1:
            subCategories = [self parseCategory:@"STEM"];
            break;
        case 2:
            subCategories = [self parseCategory:@"ANIMAL"];
            break;
        case 3:
            subCategories = [self parseCategory:@"PEOPLE"];
            break;
    }
    return subCategories;
}

- (IBAction)btnCategoryClicked:(UIButton *)sender {
    _btnDone.hidden = NO;
    _vBottomCategoryContainer.hidden = NO;
    [self makeSelectedByIndex:sender.tag];
    [self moveToCenter:sender];
}

@end
