//
//  CategoryChooser.h
//  Hiro
//
//  Created by George Vashakidze on 12/21/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Base.h"

@interface CategoryChooser : Base
@property (strong, nonatomic) IBOutlet UIView *vCategory;
@end
