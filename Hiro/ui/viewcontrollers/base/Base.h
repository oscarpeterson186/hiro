//
//  Base.h
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Events.h"
#import "HTTPWorker.h"
#import "Utils.h"

@interface Base : UIViewController <Events, UITextFieldDelegate>
/* if this flat is set to true then place a top bar with custom texts*/
@property BOOL hasTopBar;
@property (strong,nonatomic) NSString *topLeftText;
@property (strong,nonatomic) NSString *topCenterText;
@property (strong,nonatomic) UIImageView *loader;
@property (strong,nonatomic) HTTPWorker *httpWorker;

-(void) loadTopBarView;
-(void) loadDefaults;
-(void) hideKeabord;
-(void) showLoader;
-(void) hideLoader;
-(void)requestAccessToken;
@end