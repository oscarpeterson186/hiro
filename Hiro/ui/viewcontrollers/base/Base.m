//
//  Base.m
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Base.h"

@implementation Base

-(void) didLoginSucceed :(NSDictionary *) userData {
    NSLog(@"didLoginSucceed: Method should be called in child classes");
}

-(BOOL) didRegisterSucceed :(BOOL) status {
    NSLog(@"didRegisterSucceed: Method should be called in child classes");
    return NO;
}

-(void) didGetAccessTokenForRegisterSucceed :(NSDictionary *) accessTokenObj {
    NSLog(@"didGetAccessTokenForRegisterSucceed: Method should be called in child classes");
}

-(void)didCheckIfUserExistsSucceed:(NSDictionary *)userData{
    NSLog(@"didCheckIfUserExistsSucceed: Method should be called in child classes");
}

-(void) didPasswordResetSucceed :(NSDictionary *) userData{
    NSLog(@"didPasswordResetSucceed: Method should be called in child classes");
}

-(void) didGettingCategoriesSucceed :(NSDictionary *) categories{
    NSLog(@"didGettingCategoriesSucceed: Method should be called in child classes");
}

-(void)loadDefaults{}

- (void)viewDidLoad {
    
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                     initWithTarget:self
                                     action:@selector(hideKeabord)]];
    [self setNeedsStatusBarAppearanceUpdate];
    if (self.hasTopBar) {
        [self loadTopBarView];
    }
    _loader = getAnimatedLoader(self);
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self loadDefaults];
    _httpWorker = [HTTPWorker instance];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self reAssignDelegate];
}

-(void) reAssignDelegate{
   _httpWorker.eventManager = self;
}

-(void)showLoader {
    [self.view addSubview:self.loader];
}

-(void)hideLoader{
    [self.loader removeFromSuperview];
}

-(void) hideKeabord{
    [self.view endEditing:YES];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void) loadTopBarView{
    
}

-(void)requestAccessToken{
    [self showLoader];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithCapacity:3];
    [params setObject: Models.User.CLIENT_CREDENTIALS forKey: Models.User.GRANT_TYPE];
    [params setObject: Models.User.CLIENT_ID_VALUE forKey: Models.User.CLIENT_ID];
    [params setObject: Models.User.CLIENT_SECRET_VALUE forKey: Models.User.CLIENT_SECRET];
    [self.httpWorker getAccessTokenForRegister:params];
}

@end
