//
//  Alert.m
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Alert.h"

@interface Alert ()
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIImageView *alertImageView;
@property (weak, nonatomic) IBOutlet UIButton *outletClose;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) UIColor *textColor;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) UIColor *alertColor;
@property (weak, nonatomic) NSString *labelText;
@property (weak, nonatomic) NSString * alertTitle;
@property (weak, nonatomic) NSString *imageName;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property  BOOL *useBlar;
@end

@implementation Alert

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadBlurEffect];
    [self loadAlert];

}

-(void) setColors :(AlertStyle *)style :(NSString *)title :(NSString *)text :(BOOL *) mUseBlar  {
    _labelText = text;
    _useBlar = mUseBlar;
    _alertTitle = title;
    UIColor *darkColor = colorFromARGBString(Colors.ERROR_BACK_COLOR);
    if (style == DARK) {
        _textColor = [UIColor whiteColor];
        _alertColor = darkColor;
    }else{
        _textColor = darkColor;
        _alertColor = [UIColor whiteColor];
    }
}

- (IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.view removeFromSuperview];
   
}



-(void) loadAlert {
    _alertView.hidden = NO;
    _alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    _alertView.layer.cornerRadius = 6;
    _alertView.layer.masksToBounds = YES;
    _alertLabel.text = _labelText;
    _alertImageView.image = [UIImage imageNamed:@"icondanger"];
    _alertLabel.textColor = _textColor;
    _alertView.backgroundColor = _alertColor;
    _alertView.alpha = 0.88;
    _lblTitle.text = _alertTitle;
    [_outletClose setTitleColor :_textColor forState:UIControlStateNormal];
    [UIView animateWithDuration:0.3/1.5 animations:^{
        _alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            _alertView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3/2 animations:^{
                _alertView.transform = CGAffineTransformIdentity;
            }];
        }];
    }];
}

-(void) loadBlurEffect {
    if(!_useBlar) return;
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.view addSubview:blurEffectView];
        [self.view bringSubviewToFront:_alertView];
        self.view.alpha = 0.95;
    }
    else {
        self.view.backgroundColor = [UIColor blackColor];
    }
}


@end
