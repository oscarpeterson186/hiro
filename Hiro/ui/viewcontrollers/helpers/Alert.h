//
//  Alert.h
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnumConstants.h"
#import "utils.h"

@interface Alert : UIViewController

-(void) setColors :(AlertStyle *)style :(NSString *)title :(NSString *)text :(BOOL *) mUseBlar;

@end
