//
//  AppDelegate.h
//  Hiro
//
//  Created by George Vashakidze on 12/7/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

