//
//  Events.h
//  Hiro
//
//  Created by George Vashakidze on 12/10/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Events <NSObject>

-(void) didLoginSucceed :(NSDictionary *) userData;
-(BOOL) didRegisterSucceed :(BOOL) status;
-(void) didGetAccessTokenForRegisterSucceed :(NSDictionary *) accessTokenObj;
-(void) didCheckIfUserExistsSucceed :(NSDictionary *) userData;
-(void) didPasswordResetSucceed :(NSDictionary *) userData;
-(void) didGettingCategoriesSucceed :(NSDictionary *) categories;
@end
