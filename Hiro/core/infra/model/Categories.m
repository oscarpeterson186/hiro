//
//  Categories.m
//  Hiro
//
//  Created by George Vashakidze on 12/9/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "Categories.h"

@implementation Categories

+(Categories *) instance {
    static dispatch_once_t pred;
    static Categories* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}

-(void) initWithCache :(NSDictionary *) cache {

}

-(void) reloadCacheAutomatically {
    //Check if there is some changes on the server
    //Update cache
    //Do it automatically every per hour
}

-(void) forceReloadCache {
    //Let user to update cache manually
}

@end
