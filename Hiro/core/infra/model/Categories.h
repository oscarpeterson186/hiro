//
//  Categories.h
//  Hiro
//
//  Created by George Vashakidze on 12/9/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Categories : NSObject

/* Creates a singleton instance of Categories object */
+(Categories *) instance;


/* gets or sets current user categories. */
@property (strong,nonatomic) NSDictionary* categories;

@property (strong,nonatomic) NSDictionary* ANIMALS;
@property (strong,nonatomic) NSDictionary* STEM;
@property (strong,nonatomic) NSDictionary* PEOPLE;
@property (strong,nonatomic) NSDictionary* PLANET;

/* 
    After data is saved into cache, on app start place a check
    if the cache exist in shared memory then load categories from cache
 */
-(void) initWithCache :(NSDictionary *) cache;

/*
    This method will be called periodically, depended on setting value 
    It will download categories from server and save it in shared memory
 */
-(void) reloadCacheAutomatically;

/*
    This method will force download the categories from server
    and save it in shared memory
 */
-(void) forceReloadCache;



@end
