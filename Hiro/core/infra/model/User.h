//
//  User.h
//  Hiro
//
//  Created by George Vashakidze on 12/8/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface User : NSObject

/* Creates a singleton instance of User object */
+(User *) instance;

/* gets or sets current logged in user. */
@property (strong,nonatomic) NSDictionary* currentUser;

/* 
    gets current logged in user's access token.
 */
-(NSString *) getUserAccessToken;

@end
