//
//  User.m
//  Hiro
//
//  Created by George Vashakidze on 12/8/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "User.h"

@implementation User

+(User *) instance {
    static dispatch_once_t pred;
    static User* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}

-(NSString *) getUserAccessToken {
    return _currentUser != nil ? _currentUser[Models.User.ACCESS_TOKEN] : nil;
}

@end
