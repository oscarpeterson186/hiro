//
//  LemService.m
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import "LemService.h"
#import "Constants.h"

@implementation LemService

-(void) getObjects :(NSDictionary *) attributes :(void(^)(NSDictionary* objects, NSError* error)) completion {
    NSString * reqKey = attributes[RequestKeys.REQUEST_KEY];
    NSString * uriParams = [attributes objectForKey:RequestKeys.URI_PARAMS];
    NSString * body = attributes[RequestKeys.BODY];
    NSDictionary * headers = attributes[RequestKeys.HEADERS];
    LMReqObj* request = [LMReqObj create:reqKey];
    request.body = body;
    request.reqConfig.headers = headers;
    if(uriParams != nil)
       request.reqUrl = [request.reqUrl stringByAppendingString:uriParams];
    [super send:request completion:^(id object, NSError* error){
        if (error!=nil) {
            completion(nil, error);
        } else {
            NSDictionary* objects = getJSONObjFromData(object);
            completion(objects, nil);
        }
    }];
}

@end
