//
//  LemServiceBase.m
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import "LemServiceBase.h"

@implementation LemServiceBase

-(id)init {
    if (self = [super init]) {
        self->httpMgr = [LMHttpRequestManager instance];
    }
    return  self;
}

-(void) send:(LMReqObj*)req completion:(void(^)(id object, NSError* error)) completion {
    [self->httpMgr sendAsync:req completion:^(NSData* data, NSURLResponse* response, NSError* error) {
        if (response != nil) {
           #warning: Refactor this.
            NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
            NSUInteger statusCode = httpResponse.statusCode;
            if(statusCode >= 200 && statusCode < 300){
                if (statusCode == 200) {
                    completion(data,error);
                } else {
                    completion(nil,error);
                }
            }else if(statusCode >= 300 && statusCode < 400){
                completion(nil, error);
            }else if(statusCode >= 400 && statusCode < 500){
                completion(nil, error);
            }else if(statusCode >= 500 && statusCode < 600){
                completion(nil, error);
            }
        } else {
            completion(nil, error);
        }
    }];
}

@end
