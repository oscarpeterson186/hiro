//
//  LemService.h
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import "LemServiceBase.h"

@interface LemService : LemServiceBase

-(void) getObjects :(NSDictionary *) attributes :(void(^)(NSDictionary* objects, NSError* error)) completion;

@end


