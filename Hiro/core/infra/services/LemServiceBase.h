//
//  LemServiceBase.h
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import "LMHttpRequestManager.h"
#import "utils.h"

@interface LemServiceBase : NSObject {
    LMHttpRequestManager * httpMgr;
}

-(void) send:(LMReqObj*)req completion:(void(^)(id object, NSError* error)) completion;

@end
