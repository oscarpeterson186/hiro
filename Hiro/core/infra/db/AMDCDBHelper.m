//
//  AMDCDBHelper.m
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/19/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import "AMDCDBHelper.h"

NSString* const LOGDB_NAME  = @"logdb.sqlite";
NSString* const USERDB_NAME = @"datadb.sqlite";
NSString* const DB_PATH = @"/db/";

@interface AMDCDBHelper()
{
    FMDatabaseQueue *dbQueue;
    BOOL            isConnectionOpened;
    BOOL            isInitialized;
    BOOL            cacheSqls;
}

@end

// TODO: We need to add isInitialized check to all the internal functions,
// because we don't want to return null from initializer when database queue
// is null (reasons can be non existing database or closed connection)
// This time iOS skips it without exception and anyway tries to execute statements.

@implementation AMDCDBHelper

+(AMDCDBHelper *) logDbInstance {
    return [[self alloc] initLogDb];
}

+(AMDCDBHelper *) dataDbInstance {
    return [[self alloc] initDataDb];
}

-(id)initLogDb {
    
    self = [super init];
    if(self!=nil) {
        self->dbQueue = [self initialize:LOGDB_NAME];
    }
    
    return self;
}

-(id)initDataDb {
    
    self = [super init];
    if(self!=nil) {
        self->dbQueue = [self initialize:USERDB_NAME];
    }
    
    return self;
}

+(AMDCDBHelper*) getLogDb {
    return [self getLogDb:false];
}

+(AMDCDBHelper*) getLogDb:(BOOL) reopen {
    
    static AMDCDBHelper* logDb = nil;
    
    if ( logDb == nil || reopen == true) {
        
        AMDCErrorObj *error = nil;
        if(logDb!=nil) {
            [logDb closeConnection:&error];
            if(error!=nil) {
                NSLog(@"AMDCDBHelper: Failed to close database connection: %@", error.message);
            }
        }
        
        logDb =  [AMDCDBHelper logDbInstance];
        if(logDb->dbQueue!=nil) {
            AMDCErrorObj *error = nil;
            BOOL ok = [logDb openConnection:&error];
            if(!ok && error!=nil) {
                NSLog(@"AMDCDBHelper: Failed to open database connection: %@", error.message);
            }
        } else {
            return nil;
        }
        
    }
    return logDb;
}

+(AMDCDBHelper*) getUserDb {
    return [AMDCDBHelper getUserDb:false];
}

+(AMDCDBHelper*) getUserDb:(BOOL)reopen {
    static AMDCDBHelper* userDb = nil;
    
    if ( userDb == nil || reopen ==true) {
        
        AMDCErrorObj *error = nil;
        if(userDb!=nil) {
            BOOL ok = [userDb closeConnection:&error];
            if(!ok && error!=nil) {
                NSLog(@"AMDCDBHelper: Failed to open database connection: %@", error.message);
            }
        }
        
        userDb =  [AMDCDBHelper dataDbInstance];
        if(userDb->dbQueue!=nil) {
            BOOL ok = [userDb openConnection:&error];
            if(!ok && error!=nil) {
                NSLog(@"AMDCDBHelper: Failed to open database connection: %@", error.message);
            }
        } else {
            return nil;
        }
        
    }
    
    return userDb;
}

- (void)inDatabase:(void (^)(FMDatabase *db))block {
    [self->dbQueue inDatabase:block];
}

-(FMDatabaseQueue *) initialize : (NSString *) databaseName {
    NSString        *dbRelativePath = [DB_PATH stringByAppendingPathComponent:databaseName];
    NSString        *dbFullPath     = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true) objectAtIndex:0]  stringByAppendingPathComponent:dbRelativePath];
    AMDCFileManager *fileManager    = [AMDCFileManager instance];
    AMDCErrorObj *error;
    if([fileManager localFileExists:dbRelativePath] ==true|| [fileManager installPackageFile:databaseName andDestination:dbRelativePath andError:&error]==true) {
        self->isInitialized = true;
    } else {
        NSLog(@"AMDCDBHelper: Failed to initialize database: %@", error.message);
        return nil;
    }
    
    return [FMDatabaseQueue databaseQueueWithPath:dbFullPath];
}

-(BOOL) openConnection: (AMDCErrorObj **) error {
    
    __block BOOL result = true;
    
    if(dbQueue == nil) {
        return false;
    }
    __weak AMDCDBHelper* wSelf = self;
    [dbQueue inDatabase:^(FMDatabase *db) {
        AMDCDBHelper *strongSelf = wSelf;
        if([db open]==true) {
            self->isConnectionOpened = true;
        } else {
            NSString *message = [NSString stringWithFormat:@"Failed to open database connection, either database was not initialized correctly or connection is already opened, error: %@", [db lastErrorMessage]];
            NSString *description = [NSString stringWithFormat:@"AMDCDBHelper, openConnection, [db open], error message : %@", [db lastErrorMessage]];
            *error = [AMDCErrorObj error:AMDC_ERR_DB_CONN_OPEN_FAILED message:message description:description];
            result = false;
        }
    }];
    
    return result;
}

-(BOOL) openConnection : (int) flags andError : (AMDCErrorObj **) error {
    
    __block BOOL result = true;
    
    if(dbQueue == nil) {
        return false;
    }
    
    __weak AMDCDBHelper* wSelf = self;
    [dbQueue inDatabase:^(FMDatabase *db) {
        AMDCDBHelper *strongSelf = wSelf;
        if([db openWithFlags:flags]==true) {
            self->isConnectionOpened = true;
        } else {
            NSString *message = @"Failed to open database connection, either database was not initialized correctly or connection is already opened, error: %@";
            NSString *desc = [NSString stringWithFormat:@"AMDCDBHelper, openConnection, [db openWithFlags:%i], error message : %@", flags, [db lastErrorMessage]];
            *error = [AMDCErrorObj error:AMDC_ERR_DB_CONN_OPEN_FAILED message:message description:desc];
            result = false;
        }
    }];
    
    return result;
}

-(void) cacheSqlStatements:(BOOL)bCache {
    [dbQueue inDatabase:^(FMDatabase *db) {
        [self->dbQueue cacheSql:bCache];
    }];
}

-(int) freeDbMemory {

    __block int freedBytes = 0;
    [dbQueue inDatabase:^(FMDatabase *db) {
        
        freedBytes = sqlite3_db_release_memory( [db sqliteHandle] );
        NSLog(@"AMDCDBHelper: SQLite freed %d bytes", freedBytes);
        
    }];

    return freedBytes;
}

-(BOOL) beginTransaction : (AMDCErrorObj **) error{
    
    __block BOOL result = true;
    
    if(dbQueue == nil) {
        return false;
    }
    
    __weak AMDCDBHelper* wSelf = self;
    [dbQueue inDatabase:^(FMDatabase *db) {
        AMDCDBHelper *strongSelf = wSelf;
        if([db beginTransaction]==false) {
            
            NSString *message = @"AMDCDBHelper: Failed to create database transaction";
            NSString *desc    = [NSString stringWithFormat:@"AMDCDBHelper, beginTransaction, [db beginTransaction], error message : %@", [db lastErrorMessage]];
            *error = [AMDCErrorObj error:AMDC_ERR_DB_TRANS_CREATE_FAILED message:message description:desc];
            result = false;
        }
    }];
    
    return result;
}

-(BOOL) commitTransaction : (AMDCErrorObj **) error {
    
    __block BOOL result = true;
    
    if(self->dbQueue == nil) {
        return false;
    }
    
    __weak AMDCDBHelper* wSelf = self;
    [dbQueue inDatabase:^(FMDatabase *db) {
        AMDCDBHelper *strongSelf = wSelf;
        if([db commit]==false) {
            
            NSString *message = @"AMDCDBHelper: Failed to commit database transaction";
            NSString *desc    = [NSString stringWithFormat:@"AMDCDBHelper, commitTransaction, [db commit], error message : %@", [db lastErrorMessage]];
            NSLog(@"AMDCDBHelper: Failed to commit transaction: %@, error description was:\n %@", message, desc);
            *error = [AMDCErrorObj error:AMDC_ERR_DB_TRANS_COMMIT_FAILED message:message description:desc];
            result = false;
        }
    }];
    
    return result;
}

-(BOOL) rollbackTransaction: (AMDCErrorObj **) error {
    
    __block BOOL result = true;
    
    if(self->dbQueue == nil) {
        return false;
    }
    
    __weak AMDCDBHelper* wSelf = self;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        AMDCDBHelper *strongSelf = wSelf;
        if([db rollback]==false) {
            
            NSString *message   = @"AMDCDBHelper: Failed to rollback database transaction";
            NSString *desc      = [NSString stringWithFormat:@"AMDCDBHelper, rollbackTransaction, [db rollback], error message : %@", [db lastErrorMessage]];
            *error              = [AMDCErrorObj error:AMDC_ERR_DB_TRANS_ROLLBACK_FAILED message:message description:desc];
            
            result = false;
        }
    }];
    
    return result;
}

-(FMResultSet *) executeQuery : (NSString *) queryStr {
    
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeQuery:queryStr];
    }];
    
    return rs;
}

-(NSArray *) execQuery : (NSString *) queryStr {
    
    NSMutableArray *result = [NSMutableArray new];
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        rs = [db executeQuery:queryStr];
        
        while ([rs next]) {
            [result addObject:[rs resultDictionary]];
        }
        
    }];
    
    return result;
}

-(FMResultSet *) executeQuery : (NSString *) queryStr withArgumentsInArray:(NSArray *)arguments{
    
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        
        rs = [db executeQuery:queryStr withArgumentsInArray:arguments];
    }];
    
    return rs;
}

-(NSArray *) execQuery : (NSString *) queryStr withArgumentsInArray:(NSArray *)arguments{
    
    NSMutableArray *result = [NSMutableArray new];
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        
        rs = [db executeQuery:queryStr withArgumentsInArray:arguments];
        while ([rs next]) {
            [result addObject:[rs resultDictionary]];
        }
        [rs close];
    }];
    
    return result;
}

-(FMResultSet *) executeQuery : (NSString *) queryStr withParameterDictionary:(NSDictionary *)arguments{
    
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db){
        
        rs = [db executeQuery:queryStr withParameterDictionary:arguments];
    }];
    
    return rs;
}

-(NSArray *) execQuery : (NSString *) queryStr withParameterDictionary:(NSDictionary *)arguments{
    
    NSMutableArray *result = [NSMutableArray new];
    __block FMResultSet *rs = nil;
    [self->dbQueue inDatabase:^(FMDatabase *db){
        
        rs = [db executeQuery:queryStr withParameterDictionary:arguments];
        while ([rs next]) {
            [result addObject:[rs resultDictionary]];
        }
    }];
    
    return result;
}

-(long) executeInsert : (NSString *) sqlStr withArgumentsInArray:(NSArray *)arguments {

    __block long result = 0;
    if([sqlStr containsString:@"INSERT"]) {

        [self->dbQueue inDatabase:^(FMDatabase *db) {
            if([db executeUpdate:sqlStr withArgumentsInArray:arguments]) {
                result = [db lastInsertRowId];
            }
        }];
    } else {
        NSLog(@"AMDCDBHelper: No other operations are supported");
    }

    return result;
}

-(long) executeInsert : (NSString *) sqlStr withParameterDictionary:(NSDictionary *)arguments {

    __block long result = 0;
    if([sqlStr containsString:@"INSERT"]) {

        [self->dbQueue inDatabase:^(FMDatabase *db) {
            if([db executeUpdate:sqlStr withParameterDictionary:arguments]) {
                result = [db lastInsertRowId];
            }
        }];
    } else {
        NSLog(@"AMDCDBHelper: No other operations are supported");
    }

    return result;
}

-(BOOL) executeUpdate : (NSString *) sqlStr withArgumentsInArray:(NSArray *)arguments {
    
    __block BOOL result = false;
    [self->dbQueue inDatabase:^(FMDatabase *db) {
        
        result = [db executeUpdate:sqlStr withArgumentsInArray:arguments];
    }];
    
    return result;
}

-(BOOL) executeUpdate : (NSString *) sqlStr withParameterDictionary:(NSDictionary *)arguments {
    
    __block BOOL result = false;
    [self->dbQueue inDatabase:^(FMDatabase * db) {
        
        result = [db executeUpdate:sqlStr withParameterDictionary:arguments];
        
    }];
    
    return result;
}

-(long)executeCustomCUD:(NSString *)queryStr values:(NSArray *) values disableTransactions:(bool)bDisableTransactions
{
    AMDCErrorObj *error = nil;
    @try {
        if (!bDisableTransactions)
            [self beginTransaction:&error];
        
        long rowNumber = -1;
        
        if ([queryStr rangeOfString:@"insert" options:NSCaseInsensitiveSearch].location == 0) {
            rowNumber = [self executeInsert:queryStr withArgumentsInArray:values];
        } else {
            rowNumber = [self executeUpdate:queryStr withArgumentsInArray:values];
        }
        
        if (!bDisableTransactions)
            [self commitTransaction:&error];
        
        return rowNumber;
    } @catch (NSException *e) {
        if (!bDisableTransactions)
            [self rollbackTransaction:&error];
        return -1;
    }
}

-(BOOL) closeConnection : (AMDCErrorObj **) error {
    
    if(dbQueue == nil) {
        return false;
    }
    
    [self->dbQueue close];
    self->isConnectionOpened = false;
    return true;
}

-(BOOL) isConnectionOpen {
    return self->isConnectionOpened;
}

-(BOOL) isDatabaseInitialized {
    return self->isInitialized;
}


-(int)getLastErrorCode {
    
    __block int code = 0;
    [self->dbQueue inDatabase:^(FMDatabase * db){
        code = [db lastErrorCode];
    }];
    
    return code;
}

-(NSString*)getLastErrorStr {
    
    __block NSString* msg = nil;
    [self->dbQueue inDatabase:^(FMDatabase * db){
        msg = [db lastErrorMessage];
    }];
    
    return msg;
}

-(int) changes {
    
    __block int changes = 0;
    [self->dbQueue inDatabase:^(FMDatabase *db){
        changes = [db changes];
    }];
    
    return changes;
}

-(long)lastInsertRowId {
    
    __block long result = 0;
    [self->dbQueue inDatabase:^(FMDatabase *db){
        result = (long)[db lastInsertRowId];
    }];
    
    return result;
}

-(void)dealloc {
    
    AMDCErrorObj *error = nil;
    self->dbQueue = nil;
    self->isConnectionOpened = false;
    self->isInitialized = false;
    self->cacheSqls = false;
    if(error!=nil) {
        NSLog(@"AMDCDBHelper: Failed to close database connection, either connection was already closed for that time or database was not initialized correctly, error: %@", error.message);
    }
    
}

@end
