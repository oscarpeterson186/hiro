//
//  AMDCDBHelper.h
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/19/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

/*!
 @header AMDCDBHelper
 @discussion Database helper class
 */

//#import "AMDCEventTarget.h"
#import "FMDatabase.h"
//#import "AMDCErrorObj.h"
#import "AMDCFileManager.h"
#import "utils.h"
#import "DBTypes.h"
#import "FMDatabaseQueue.h"
#import "ErrorCodes.h"

/*!
 * @interface AMDCDBHelper
 * @discussion AMDC Database helper class works on user (datadb.sqlite) and log (logdb.sqlite) databases.
 * For thread safety CRUD operations are queued and performed one by one.
 * @updated 2014-06-03
 */
@interface AMDCDBHelper : NSObject

/** Public API declaration Start **/

/*!
* @function getLogDB
* @discussion Singleton type function returns AMDCDBHelper instance for log database
* @return AMDCDBHelper instance for log database.
*/
+(AMDCDBHelper*) getLogDb;

/*!
 * @function getLogDB
 * @discussion Singleton type function returns AMDCDBHelper instance for log database.
 * If reopen parameter is true, creates the new instance
 * @return AMDCDBHelper instance for log database.
 */
+(AMDCDBHelper*) getLogDb:(BOOL) reopen;

/*!
 * @function getUserDb
 * @discussion Singleton type function returns AMDCDBHelper instance for user database.
 * @return AMDCDBHelper instance for user database.
 */
+(AMDCDBHelper*) getUserDb;

/*!
 * @function getUserDb
 * @discussion Singleton type function returns AMDCDBHelper instance for user database.
 * If reopen parameter is true, creates the new instance
 * @return AMDCDBHelper instance for user database.
 */
+(AMDCDBHelper*) getUserDb:(BOOL)reopen;

/*!
 * @function inDatabase
 * @discussion queues and executes passed operation block.
 * @return void
 */
-(void)inDatabase:(void (^)(FMDatabase *db))block;

/*!
 * @function openConnection
 * @discussion Opens the database connection.
 * @param Error object
 * @return Boolean value indicating whether database connection was opened successfully
 */

-(BOOL) openConnection: (AMDCErrorObj **) error;

/*!
 * @function closeConnection
 * @discussion Closes the database connection.
 * @param Error object
 * @return Boolean value indicating whether database connection was closed successfully
 */
-(BOOL) closeConnection : (AMDCErrorObj **) error;

/*!
 * @function beginTransaction
 * @discussion Creates the sql transaction
 * @param Error object
 * @return Boolean value indicating whether transaction was created successfully
 */
-(BOOL) beginTransaction : (AMDCErrorObj **) error;

/*!
 * @function commitTransaction
 * @discussion Commits the sql transaction
 * @param Error object
 * @return Boolean value indicating whether transaction was commited successfully
 */
-(BOOL) commitTransaction : (AMDCErrorObj **) error;

/*!
 * @function rollbackTransaction
 * @discussion Rolls back the sql transaction
 * @param Error object
 * @return Boolean value indicating whether transaction was rolled back successfully
 */
-(BOOL) rollbackTransaction : (AMDCErrorObj **) error;

/*!
 * @function executeQuery
 * @discussion Executes sql query and returns resultset object with cursor set to first record.
 * @param Query string
 * @return ResultSet object
 */
-(FMResultSet *) executeQuery : (NSString *) queryStr;

/*!
 * @function executeQuery
 * @discussion Executes sql query and returns result in array. (Performs loop to populate array from resultset object)
 * @param Query string
 * @return Array of records
 */
-(NSArray *) execQuery : (NSString *) queryStr;

/*!
 * @function executeQuery
 * @discussion Executes sql query with arguments in array.
 * Ex. where id in (?,?,?) -> [1,2,3]
 * @param Query string
 * @param Arguments array
 * @return ResultSet object
 */
-(FMResultSet *) executeQuery : (NSString *) queryStr withArgumentsInArray:(NSArray *)arguments;

/*!
 * @function execQuery
 * @discussion Executes sql query with arguments in array and returns result in array. (Performs loop to populate array from resultset object)
 * Ex. where id in (?,?,?) -> [1,2,3]
 * @param Query string
 * @param Arguments array
 * @return Array of records
 */
-(NSArray *) execQuery : (NSString *) queryStr withArgumentsInArray:(NSArray *)arguments;

/*!
 * @function executeQuery
 * @discussion Executes sql query with parameters map
 * @param Query string
 * @param Arguments map
 * @return ResultSet object
 */
-(FMResultSet *) executeQuery : (NSString *) queryStr withParameterDictionary:(NSDictionary *)arguments;

/*!
 * @function executeQuery
 * @discussion Executes sql query with parameters map and returns result in array. (Performs loop to populate array from resultset object)
 * @param Query string
 * @param Arguments map
 * @return Array of records
 */
-(NSArray *) execQuery : (NSString *) queryStr withParameterDictionary:(NSDictionary *)arguments;

/*!
 * @function executeUpdate
 * @discussion Executes update statement with arguments in array
 * @param Sql string
 * @param Arguments array
 * @return Boolean value indicating whether sql was successfully executed
 */
-(BOOL) executeUpdate : (NSString *) sqlStr withArgumentsInArray:(NSArray *)arguments;

/*!
 * @function executeUpdate
 * @discussion Executes update statement with arguments in map
 * @param Sql string
 * @param Arguments map
 * @return Boolean value indicating whether sql was successfully executed
 */
-(BOOL) executeUpdate :(NSString *) sqlStr withParameterDictionary:(NSDictionary *)arguments;

/*!
 * @function executeInsert
 * @discussion Executes insert statement with arguments in array
 * @param Sql string
 * @param Arguments array
 * @return Last inserted row id
 */
-(long) executeInsert : (NSString *) sqlStr withArgumentsInArray:(NSArray *)arguments;

/*!
 * @function executeInsert
 * @discussion Executes insert statement with arguments in array
 * @param Sql string
 * @param Arguments dictionary
 * @return Last inserted row id
 */
-(long) executeInsert : (NSString *) sqlStr withParameterDictionary:(NSDictionary *)arguments;

/*!
 * @function executeCustomCUD
 * @discussion Executes CUD statement with arguments in array
 * @param queryStr string
 * @param values array
 * @param bDisableTransactions bool
 * @return Last inserted row id
 */
-(long)executeCustomCUD:(NSString *)queryStr values:(NSArray *) values disableTransactions:(bool)bDisableTransactions;

/*!
 * @function isDatabaseInitialized
 * @discussion Checks if database is initialized
 * @return Boolean value
 */
-(BOOL) isDatabaseInitialized;

/*!
 * @function getLastErrorCode
 * @discussion Returns last error code generated from SQLITE engine
 * @return Error code
 */
-(int)getLastErrorCode;

/*!
 * @function getLastErrorStr
 * @discussion Returns last error message generated from SQLITE engine
 * @return Error message
 */
-(NSString*)getLastErrorStr;

/*!
 * @function lastInsertRowId
 * @discussion Returns last inserted row id
 * @return Row id
 */
-(long)lastInsertRowId;

/*!
 * @function cacheSqlStatements
 * Sets the specified boolean flag, should the sql statements be cached or not
 * @return void
 */
-(void) cacheSqlStatements:(BOOL)bCache;
-(int) freeDbMemory;

/*!
 * @function changes
 * Returns the int value of the changes affected to SQLITE, after executing any statement, INSERT/UPDATE/DELETE
 * @return int
 */
-(int) changes;

/** Public API declaration End **/

@end
