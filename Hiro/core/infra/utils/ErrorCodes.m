//
//  ErrorCodes.m
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/17/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import "ErrorCodes.h"

int const AMDC_ERR_FATAL                    = 9000;

int const AMDC_ERR_FILE_NOT_FOUND           = 9001;
int const AMDC_ERR_FILE_COPY_FAILED         = 9002;
int const AMDC_ERR_DIR_NOT_FOUND            = 9002;
int const AMDC_ERR_DIR_CREATE_FAILED        = 9003;
int const AMDC_ERR_FILE_ATTR_READ_FAILED    = 9004;
int const AMDC_ERR_FILE_DELETE_FAILED       = 9005;
int const AMDC_ERR_FILE_WRITE_FAILED        = 9006;
int const AMDC_ERR_FILE_READ_FAILED         = 9007;

int const AMDC_ERR_DB_INIT_FAILED           = 9101;
int const AMDC_ERR_DB_CONN_OPEN_FAILED      = 9102;
int const AMDC_ERR_DB_CONN_CLOSE_FAILED     = 9103;
int const AMDC_ERR_DB_TRANS_CREATE_FAILED   = 9104;
int const AMDC_ERR_DB_TRANS_COMMIT_FAILED   = 9105;
int const AMDC_ERR_DB_TRANS_ROLLBACK_FAILED = 9106;
int const AMDC_ERR_DB_UPDATE_FAILED         = 9107;



int const AMDC_ERR_HTTP_REQUEST_FAILED      = 9201;
int const AMDC_ERR_HTTP_REQUEST_FAILED_401  = 9202;
int const AMDC_ERR_HTTP_REQUEST_FAILED_500  = 9203;
int const AMDC_ERR_NULL_HTTP_REQUEST        = 9204;


int const AMDC_ERR_NO_FREE_SPACE            = 9301;
int const AMDC_ERR_ATTCHM_DNLD_FAILED       = 9302;
int const AMDC_ERR_ATTCHM_SIZE_LIMIT_EXCEED = 9303;
int const AMDC_ERR_AMDCOM_INITIALIZATION    = 9304;

/*
 * Synchronization logging error codes
 */
int const AMDC_ERR_SYNC_OBJ_NOT_FOUND_FATAL                 = 9401;
int const AMDC_ERR_SYNC_OBJ_NOT_FOUND_PENDING_DELETED       = 9402;
int const AMDC_ERR_SYNC_INSERT_FAILED                       = 9403;
int const AMDC_ERR_SYNC_UNKNOWN_RESP_OBJ_ON_PART_DNLD       = 9404;
int const AMDC_ERR_SYNC_UNEXPECTED_RESP_CODE_ON_PART_DNLD   = 9405;
int const AMDC_ERR_SYNC_ABORTED                             = 9406;
int const AMDC_ERR_SYNC_EXITONWRONGTHREAD                   = 9407;
int const AMDC_ERR_SYNC_ZEROCURRENTSYNCDATE                 = 9408;
int const AMDC_ERR_BAD_JSON                                 = 9409;
int const AMDC_ERR_UNCAIGHT_EXCEPTION                       = 9410;
int const AMDC_ERR_SYNC_DISABLED                            = 9411;

int const AMDC_ERR_NOCONNECTIVITY                           = 9412;

/*
 * Device register, backend validation etc...
 */
int const AMDC_ERR_BACKEND_VALIDATION_FAILED                = 9501;
int const CDM_UPGREADE_REQUIRED                             = 9502;
int const DUID_VALIDATION_ERROR                             = 9503;
int const IMAGE_DL_FAILED                                   = 9504;
int const AMDC_ERR_NOTIFICATION_REG_FAIL                    = 9505;
int const AMDC_ERR_LOG_UPLOAD_FAIL                          = 9506;

/*
 * Subscriptions
 */
int const AMDC_ERR_SUBSC_REQ_FAILED                         = 9601;

/*
 * Event Target
 */
int const AMDC_ERR_EVENT_FIRE_FAILED                        = 9701;


int const AMDC_INFO_SYNCPAUSED                              = 9900;

int const BLANK_ERR                                         = -1;
