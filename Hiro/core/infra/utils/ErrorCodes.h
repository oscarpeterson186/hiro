//
//  Constants.h
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/14/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

extern int const AMDC_ERR_FATAL;

extern int const AMDC_ERR_FILE_NOT_FOUND;
extern int const AMDC_ERR_FILE_COPY_FAILED;
extern int const AMDC_ERR_DIR_NOT_FOUND;
extern int const AMDC_ERR_DIR_CREATE_FAILED;
extern int const AMDC_ERR_FILE_ATTR_READ_FAILED;
extern int const AMDC_ERR_FILE_DELETE_FAILED;
extern int const AMDC_ERR_FILE_WRITE_FAILED;
extern int const AMDC_ERR_FILE_READ_FAILED;
extern int const AMDC_ERR_NO_FREE_SPACE;

extern int const AMDC_ERR_DB_INIT_FAILED;
extern int const AMDC_ERR_DB_CONN_OPEN_FAILED;
extern int const AMDC_ERR_DB_CONN_CLOSE_FAILED;
extern int const AMDC_ERR_DB_TRANS_CREATE_FAILED;
extern int const AMDC_ERR_DB_TRANS_COMMIT_FAILED;
extern int const AMDC_ERR_DB_TRANS_ROLLBACK_FAILED;
extern int const AMDC_ERR_DB_UPDATE_FAILED;

extern int const AMDC_ERR_HTTP_REQUEST_FAILED;
extern int const AMDC_ERR_HTTP_REQUEST_FAILED_401;
extern int const AMDC_ERR_HTTP_REQUEST_FAILED_500;
extern int const AMDC_ERR_NULL_HTTP_REQUEST;

extern int const AMDC_ERR_ATTCHM_DNLD_FAILED;
extern int const AMDC_ERR_ATTCHM_SIZE_LIMIT_EXCEED;
extern int const AMDC_ERR_AMDCOM_INITIALIZATION;

/* 
 * Synchronization error codes
 */
extern int const AMDC_ERR_SYNC_OBJ_NOT_FOUND_FATAL;
extern int const AMDC_ERR_SYNC_OBJ_NOT_FOUND_PENDING_DELETED;
extern int const AMDC_ERR_SYNC_INSERT_FAILED;
extern int const AMDC_ERR_SYNC_UNKNOWN_RESP_OBJ_ON_PART_DNLD;
extern int const AMDC_ERR_SYNC_UNEXPECTED_RESP_CODE_ON_PART_DNLD;
extern int const AMDC_ERR_SYNC_ABORTED;
extern int const AMDC_ERR_SYNC_EXITONWRONGTHREAD;
extern int const AMDC_ERR_SYNC_ZEROCURRENTSYNCDATE;
extern int const AMDC_ERR_BAD_JSON;
extern int const AMDC_ERR_UNCAIGHT_EXCEPTION;
extern int const AMDC_ERR_SYNC_DISABLED;

extern int const AMDC_ERR_NOCONNECTIVITY;


/*
 * Device register, backend validation etc...
 */
extern int const AMDC_ERR_BACKEND_VALIDATION_FAILED;
extern int const CDM_UPGREADE_REQUIRED;
extern int const DUID_VALIDATION_ERROR;
extern int const IMAGE_DL_FAILED;
extern int const AMDC_ERR_NOTIFICATION_REG_FAIL;
extern int const AMDC_ERR_LOG_UPLOAD_FAIL;


/*
 * Subscriptions
 */
extern int const AMDC_ERR_SUBSC_REQ_FAILED;

/*
 * Event Target
 */
extern int const AMDC_ERR_EVENT_FIRE_FAILED;

extern int const AMDC_INFO_SYNCPAUSED;

extern int const BLANK_ERR;
