//
//  NSString+NSStringCategory.h
//  AMDCom
//
//  Created by Irakli Vashakidze on 10/15/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (AMDCNSString)

-(NSString *) substringUpToFirstOccurance : (char) c;
-(NSString *) substringAfterFirstOccurance : (NSString*) str;
-(NSString *) substringFromFirstOccurance : (char) c;
-(NSString *) substringUpToLastOccurance : (char) c;
-(NSString *) substringFromRightFromCharacter : (char) c;
-(NSString *) substringFromLastOccurance: (NSString *) str;

-(NSUInteger)indexOf:(char)c;
- (BOOL) containsString: (NSString*) substring;

@end
