//
//  NSString+NSStringCategory.m
//  AMDCom
//
//  Created by Irakli Vashakidze on 10/15/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import "NSString+AMDCNSString.h"

@implementation NSString (AMDCNSString)

-(NSString *) substringUpToFirstOccurance : (char) c {
    NSRange range = [self rangeOfString:[NSString stringWithFormat:@"%c", c]];
    return range.location != NSNotFound ? [self substringFromIndex:range.location+1] : self;
}

-(NSString *) substringAfterFirstOccurance : (NSString*) str {
    NSRange range = [self rangeOfString: str];
    return range.location != NSNotFound ? [self substringFromIndex:range.location+1] : self;
//    NSArray *splitedArr = [self componentsSeparatedByString:str];
//    return [splitedArr count] > 1 ? splitedArr[1] : self;
}

-(NSString *) substringFromFirstOccurance : (char) c {
    NSRange range = [self rangeOfString:[NSString stringWithFormat:@"%c", c]];
    return [self substringToIndex:range.location];
}

-(NSString *) substringUpToLastOccurance : (char) c {
    NSRange range = [self rangeOfString:[NSString stringWithFormat:@"%c", c] options:NSBackwardsSearch];
    return [self substringFromIndex:range.location+1];
}

-(NSString *) substringFromRightFromCharacter : (char) c {
    NSRange range = [self rangeOfString:[NSString stringWithFormat:@"%c", c] options:NSBackwardsSearch];
    return [self substringToIndex:range.location];
}

-(NSString *) substringFromLastOccurance: (NSString *) str {
    NSRange range = [self rangeOfString:str];
    return [self substringFromIndex:(range.location + range.length)];
}

-(NSUInteger)indexOf:(char)c {
	
    NSRange range = [self rangeOfString:[NSString stringWithFormat:@"%c", c] options:NSBackwardsSearch];
    return range.location;
}

- (BOOL) containsString: (NSString*) substring
{
    NSRange range = [self rangeOfString : substring];
    BOOL found = ( range.location != NSNotFound );
    return found;
}

@end
