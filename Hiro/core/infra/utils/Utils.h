//
//  utils.h
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Alert.h"
#import "Constants.h"
#import "UncaughtExceptionHandler.h"
#import <FLAnimatedImage.h>

typedef enum {
    none,
    wifi,
    mobile,
    wimax,
    ethernet
}  ConnectivityType;

NSString* LocalizedString(NSString* key, NSString* comment);
NSString* base64String(NSString* str);
NSString* escapeString4JSON(NSString* src);
NSString* getJSONStr (id jsonObj);
id getJSONObj (NSString* jsonStr);
id getJSONObjFromData (NSData* jsonData);

NSString* date2MilliSecStr(NSDate* date);
long long date2MilliSecs(NSDate* date);

NSString* quoteString(NSString* src);
NSString* id2JSONStr(id newValue);
NSString* serializeURIParams (NSDictionary * params);

/**
 returns json string or nil for cdmTypeDef, newFieldValuesMap, oldFieldValuesMap // (without AMDCRowId)
 what is formed for changelog data column.
 */
NSString* createChangeLogDataJSONStr(NSDictionary *cdmTypeDef, NSDictionary *newFieldValues, NSDictionary *oldFieldValues);

/**
 matches-contains or not target str a regular expression pattern
 
 @param regex - what to search in regular expression pattern
 @param targetStr - where to search
 @result - bool
 */
BOOL containsRegExprStr(NSString * regex, NSString * targetStr);
/**
 replaces into targetStr a regular expression pattern with replaceWithStr
 
 @param regexpStr - what to search in regular expression pattern
 @parAM replaceWithStr  - replacement string
 @param targetStr - where to search
 @result - replaced NSString
 */
NSString* replaceRegexpStr(NSString *regexpStr, NSString *replaceWithStr, NSString *targetStr);
NSString* arrTOstr(NSArray *arr) ;

/*!
 * @function colorFromARGBString
 * @discussion receives ARGB string in format #AARRGGBB where each byte is hex value for coresponding part,
 decodes and createsUI color based on input string.
 * @param ARGB string prefixed with #
 * @return UIColor object from input string
 */
UIColor* colorFromARGBString(NSString* argbColorStr);

/*!
 * @function colorFromARGB
 * @discussion receives ARGB int in format #AARRGGBB where each byte is hex value for coresponding part,
 decodes and createsUI color based on input string.
 * @param ARGB int
 * @return UIColor object from input string
 */
UIColor* colorFromARGB(unsigned int argbColor);

NSString* getFieldAlias(NSString* field);
NSString* getFieldAliasForType(NSString* type, NSString* field);
void handleException(NSException* exception, NSString* src);
bool InstallAMDCUncaughtExceptionHandler();
void setTextInputStartPosition(UITextField *field);
void setButtonTextUnderlineStyle(UIButton * button);
UIActivityIndicatorView * getLoader(UIViewController *v);
void launchModal(UIViewController *v, NSString * title, NSString * text, AlertStyle style, BOOL *showBlur);
UIImageView *getAnimatedLoader(UIViewController *v);
void setButtonTextUnderlineStyleWithTitle(UIButton * button, NSString *title);