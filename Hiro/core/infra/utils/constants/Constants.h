//
//Constants.h
//Hiro
//
//CreatedbyGeorgeVashakidzeon12/7/15.
//Copyright©2015Lemondo.Allrightsreserved.
//

#import<Foundation/Foundation.h>


/******************DEVICE TYPES*********************/
#define IS_OS_8_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_4 (IS_IPHONE && ([[UIScreen mainScreen] bounds].size.height == 480.0) && ((IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale == [UIScreen mainScreen].scale) || !IS_OS_8_OR_LATER))
#define IS_IPHONE_5 (IS_IPHONE && ([[UIScreen mainScreen] bounds].size.height == 568.0) && ((IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale == [UIScreen mainScreen].scale) || !IS_OS_8_OR_LATER))
#define IS_STANDARD_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0  && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale == [UIScreen mainScreen].scale)
#define IS_ZOOMED_IPHONE_6 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0 && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale > [UIScreen mainScreen].scale)
#define IS_STANDARD_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_ZOOMED_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0 && IS_OS_8_OR_LATER && [UIScreen mainScreen].nativeScale < [UIScreen mainScreen].scale)
/******************DEVICE TYPES*********************/


/******************MODELS***************************/

/* This structure contains constants for online objects */
extern const struct Models {
    
    /* Categories */
    struct {
        __unsafe_unretained NSString* ANIMALS;
        __unsafe_unretained NSString* STEM;
        __unsafe_unretained NSString* PEOPLE;
        __unsafe_unretained NSString* PLANET;
    } Categories;
    
    /* Names */
    struct {
        __unsafe_unretained NSString* USER;
        __unsafe_unretained NSString* CATEGORIES;
        __unsafe_unretained NSString* REGISTER;
        __unsafe_unretained NSString* USER_PARAM;
        __unsafe_unretained NSString* CHECK_USER;
    } Names;
    
    /* User */
    struct {
        __unsafe_unretained NSString* ACCESS_TOKEN;
        __unsafe_unretained NSString* REFRESH_TOKEN;
        __unsafe_unretained NSString* TOKEN_TYPE;
        __unsafe_unretained NSString* EXPIRES_IN;
        __unsafe_unretained NSString* IDENTITY;
        __unsafe_unretained NSString* USER;
        __unsafe_unretained NSString* ID;
        __unsafe_unretained NSString* CREATED_AT;
        __unsafe_unretained NSString* UPDATED_AT;
        __unsafe_unretained NSString* DATE;
        __unsafe_unretained NSString* TIMEZONE_TYPE;
        __unsafe_unretained NSString* TIMEZONE;
        __unsafe_unretained NSString* UTC;
        __unsafe_unretained NSString* IS_ACTIVE;
        __unsafe_unretained NSString* EMAIL;
        __unsafe_unretained NSString* USERNAME;
        __unsafe_unretained NSString* USER_LOGIN;
        __unsafe_unretained NSString* NAME;
        __unsafe_unretained NSString* DESCRIPTION;
        __unsafe_unretained NSString* SHOUTOUT;
        __unsafe_unretained NSString* GENDER;
        __unsafe_unretained NSString* AGE_RANGE;
        __unsafe_unretained NSString* AVATAR;
        __unsafe_unretained NSString* IS_CONFIRMED;
        __unsafe_unretained NSString* LAST_LOGIN_AT;
        __unsafe_unretained NSString* TOTAL_HERO_CAUSES;
        __unsafe_unretained NSString* TOTAL_LIKED_CAUSES;
        __unsafe_unretained NSString* TOTAL_LIKED_CHAMPIONS;
        __unsafe_unretained NSString* TOTAL_LIKED_HEROS;
        __unsafe_unretained NSString* TOTAL_LIKES;
        __unsafe_unretained NSString* TOTAL_SHAERS;
        __unsafe_unretained NSString* WEBSITE;
        __unsafe_unretained NSString* RANK;
        __unsafe_unretained NSString* CATEGORIES;
        __unsafe_unretained NSString* CATEGORY;
        __unsafe_unretained NSString* SLUG;
        __unsafe_unretained NSString* HUMAR_RIGHTS;
        __unsafe_unretained NSString* HASH_TAGS;
        __unsafe_unretained NSString* HASH_TAG;
        __unsafe_unretained NSString* PARENT_CATEGORY;
        __unsafe_unretained NSString* PEOPLE;
        __unsafe_unretained NSString* HEALTH_WELLNESS;
        __unsafe_unretained NSString* COMMUNITY;
        __unsafe_unretained NSString* TRANSPORTATION;
        __unsafe_unretained NSString* STEM;
        __unsafe_unretained NSString* FARM_ANIMALS;
        __unsafe_unretained NSString* F_A_W_B_S;
        __unsafe_unretained NSString* ANIMAL;
        __unsafe_unretained NSString* ROLES;
        __unsafe_unretained NSString* ROLE;
        __unsafe_unretained NSString* DEVELOPER;
        __unsafe_unretained NSString* SOCIAL_ACCOUNTS;
        __unsafe_unretained NSString* SOCIAL_PAGES;
        __unsafe_unretained NSString* GRANT_TYPE;
        __unsafe_unretained NSString* PASSWORD;
        __unsafe_unretained NSString* CLIENT_CREDENTIALS;
        __unsafe_unretained NSString* CLIENT_ID;
        __unsafe_unretained NSString* CLIENT_ID_VALUE;
        __unsafe_unretained NSString* CLIENT_SECRET;
        __unsafe_unretained NSString* CLIENT_SECRET_VALUE;
        __unsafe_unretained NSString* BEANER_AUTH_HEADER_KEY;
        __unsafe_unretained NSString* BEANER_AUTH_HEADER_REGISTER_KEY;
        __unsafe_unretained NSString* USER_IS_AVAILABLE;
        __unsafe_unretained NSString* RESET_PASSWORD;
        __unsafe_unretained NSString* DATA;
    } User;
    
} Models;
/******************MODELS***************************/

/******************REQUEST CONSTANTS***************************/
/* This structure contains constants to work with HTTP requests */
extern const struct RequestKeys {
    __unsafe_unretained NSString* REQUEST_KEY;
    __unsafe_unretained NSString* BODY;
    __unsafe_unretained NSString* HEADERS;
    __unsafe_unretained NSString* AUTHORIZATION;
    __unsafe_unretained NSString* URI_PARAMS;
} RequestKeys;
/******************REQUEST CONSTANTS***************************/


/******************REQUEST METHODS***************************/
/* This structure contains constants to work with request methods */
extern const struct RequestMethods {
    __unsafe_unretained NSString* GET;
    __unsafe_unretained NSString* HEAD;
    __unsafe_unretained NSString* POST;
    __unsafe_unretained NSString* PUT;
    
    __unsafe_unretained NSString* DELETE;
    __unsafe_unretained NSString* CONNECT;
    __unsafe_unretained NSString* OPTIONS;
    __unsafe_unretained NSString* TRACE;
    
} RequestMethods;
/******************REQUEST METHODS***************************/

/******************ERROR CODES***************************/
/* This structure contains constants to work with error codes */
extern const struct ErrorCodes {
    __unsafe_unretained NSString* ERROR;
    __unsafe_unretained NSString* ATTENTION;
    __unsafe_unretained NSString* USERNAME_OR_PASSWORD_INCORRECT;
    __unsafe_unretained NSString* USERNAME_ALREADY_EXISTS;
    __unsafe_unretained NSString* EMAIL_ALREADY_EXISTS;
    __unsafe_unretained NSString* FILL_OUT_ALL_FIELDS;
    __unsafe_unretained NSString* FILL_OUT_ALL_REQUIRED_FIELDS;
    __unsafe_unretained NSString* YOUR_PROCESS_COULD_NOT_BE_COMPLETED;
    __unsafe_unretained NSString* COULD_NOT_FETCH_CATEGORIES_FROM_SERVER;
} ErrorCodes;
/******************ERROR CODES***************************/

/******************VIEW HELPERS***************************/
/* This structure contains constants to work with XIB/NIB files */
extern const struct XIBS {
    __unsafe_unretained NSString* ALERTHELPER;
} XIBS;
/******************VIEW HELPERS***************************/

extern const struct  OperationType {
    __unsafe_unretained NSString* INSERT;
    __unsafe_unretained NSString* UPDATE;
    __unsafe_unretained NSString* DELETE;
    __unsafe_unretained NSString* SUBSCRIBE;
    __unsafe_unretained NSString* UNSUBSCRIBE;
    __unsafe_unretained NSString* CUSTOM;
} OperationType;

/****************** FILE UPLOAD REQUEST ********************/
extern const struct FileUploadRequestConsts {
    
    __unsafe_unretained NSString* MIMETYPE_OCTETSTREAM;
    __unsafe_unretained NSString* MIMETYPE_JSON;
    __unsafe_unretained NSString* CNTENCODING_BINARY;
    __unsafe_unretained NSString* CNTENCODING_8BIT;
    __unsafe_unretained NSString* MULTIPART_DATA_STRINGFMT;
    __unsafe_unretained NSString* MULTIPART_FNAME_STRINGFMT;
    
} FileUploadRequestConsts;

extern const struct DbTypeNames {
    
    __unsafe_unretained NSString* DECIMAL;
    __unsafe_unretained NSString* VARCHAR;
    __unsafe_unretained NSString* DATETIME;
    __unsafe_unretained NSString* TEXT;
    __unsafe_unretained NSString* INTEGER;
    __unsafe_unretained NSString* DATA;
    __unsafe_unretained NSString* CHARACTER;
    
} DbTypeNames;

extern const struct ContentTypes {
    
    __unsafe_unretained NSString* CURRENCY;
} ContentTypes;

extern const struct DateTypes {
    
    __unsafe_unretained NSString* DATETIME;
    __unsafe_unretained NSString* DATE;
    __unsafe_unretained NSString* TIME;
} DateTypes;

/**
 Supported SQLite aggregate function names.
 NOTE - callers must use the constants below, passing strings with equivalent values will not work!
 */
extern const struct SqlAggKeyWords {
    
    __unsafe_unretained NSString* AGG_COUNT;
    __unsafe_unretained NSString* AGG_SUM;
    __unsafe_unretained NSString* AGG_AVG;
    __unsafe_unretained NSString* AGG_MIN;
    __unsafe_unretained NSString* AGG_MAX;
    
} SqlAggKeyWords;

extern const struct SqlGenConsts {
    
    __unsafe_unretained NSString* CL_SELECT;
    __unsafe_unretained NSString* CL_INSERT;
    __unsafe_unretained NSString* CL_UPDATE;
    __unsafe_unretained NSString* CL_DELETE;
    
    __unsafe_unretained NSString* CL_VALUES;
    __unsafe_unretained NSString* CL_SET;
    __unsafe_unretained NSString* CL_OPENPRN;
    __unsafe_unretained NSString* CL_CLOSEPRN;
    
    __unsafe_unretained NSString* CL_FROM;
    __unsafe_unretained NSString* CL_JOIN;
    __unsafe_unretained NSString* CL_ON;
    __unsafe_unretained NSString* CL_WHERE;
    __unsafe_unretained NSString* CL_GROUPBY;
    __unsafe_unretained NSString* CL_ORDERBY;
    __unsafe_unretained NSString* CL_LIMIT;
    __unsafe_unretained NSString* CL_OFFSET;
    
    __unsafe_unretained NSString* OP_EQ;
    __unsafe_unretained NSString* OP_LIKE;
    __unsafe_unretained NSString* OP_OR;
    __unsafe_unretained NSString* OP_AND;
    __unsafe_unretained NSString* SQL_APOSTR;
    __unsafe_unretained NSString* SQL_STAR;
    __unsafe_unretained NSString* SQL_PCT;
    __unsafe_unretained NSString* SQL_QMARK;
    __unsafe_unretained NSString* SQL_UNDER;
    __unsafe_unretained NSString* REGEXP_STAR;
    __unsafe_unretained NSString* REGEXP_QMARK;
    
    __unsafe_unretained NSString* ESCAPED_STAR;
    __unsafe_unretained NSString* ESCAPED_QMARK;
    
    __unsafe_unretained NSString* SQL_DOT;
    __unsafe_unretained NSString* SQL_COMA;
    __unsafe_unretained NSString* SQL_FWDSLASH;
    
    __unsafe_unretained NSString* LIKE;
    
    __unsafe_unretained NSString* ALIAS_SEP;
    
} SqlGenConsts;

extern const struct SqlGen {

/*************************************** SqlGen info/wrn/Err messages for DLOG *****************************************************/
    struct {
        __unsafe_unretained NSString* illegalInputParams;
    } Err;
    
} SqlGen;

/************ SEARCH SPEC *************************/

extern const struct SearchSpecConsts {
    __unsafe_unretained NSString* OPENING_PARENTHESIS;
    __unsafe_unretained NSString* CLOSING_PARENTHESIS;
    __unsafe_unretained NSString* AND;
    __unsafe_unretained NSString* OR;
} SearchSpecConsts;

extern const struct Files {
    
    __unsafe_unretained NSString* SETTINGSFILE;
    __unsafe_unretained NSString* AMDCOM_RES_BUNDLE;
    __unsafe_unretained NSString* CDM_SERVER_FILE_NAME;
    __unsafe_unretained NSString* CDM_LOCAL_FILE_NAME;
    __unsafe_unretained NSString* METADATA_DIR_PATH;
    __unsafe_unretained NSString* UID_FILE_NAME;
    __unsafe_unretained NSString* DB_DIR_PATH;
    __unsafe_unretained NSString* DB_FILE_NAME;
    
    __unsafe_unretained NSString* PATH_ATTACHMENTDIR;
    __unsafe_unretained NSString* PATH_AMDCTEMPDIR;
    
    struct {
        __unsafe_unretained NSString* KEY_DOWNLOAD_ATTACHMENT_ID;
        __unsafe_unretained NSString* KEY_UPLOAD_ATTACHMENT_ID;
        __unsafe_unretained NSString* KEY_ATTACHMENTTYPE;
        __unsafe_unretained NSString* KEY_FILENAME;
        __unsafe_unretained NSString* KEY_REQUESTPARAMS;
    } Keys;
    
} Files;

extern const struct Errors {
    
    __unsafe_unretained NSString* EXCEPTION_CLASS_NAME;
    __unsafe_unretained NSString* MESSAGE;
    __unsafe_unretained NSString* EXCEPTION;
    
} Errors;

extern const struct Successes {
    __unsafe_unretained NSString* SUCCESS;
} Successes;

extern const struct DaoConsts {
    __unsafe_unretained NSString* OBJECT;
    __unsafe_unretained NSString* LAST_ERROR_CODE;
    __unsafe_unretained NSString* LAST_ERROR_MESSAGE;
    __unsafe_unretained NSString* LOGGING_INTO_CHANGELOG_FAILED;
    __unsafe_unretained NSString* CONCURENTUPDATEERRORMSG;
    // id field name
    __unsafe_unretained NSString* NAME_FIELDMAP ;
    __unsafe_unretained NSString* IDENTIFIER;
    __unsafe_unretained NSString* ROWID;
    
    // consts for CRUDs log strings
    __unsafe_unretained NSString* LOG_SELECT ;
    __unsafe_unretained NSString* LOG_INSERT ;
    __unsafe_unretained NSString* LOG_UPDATE ;
    __unsafe_unretained NSString* LOG_DELETE ;
    
    
    // consts for event DataObject's property names
    __unsafe_unretained NSString* PRK_FIELD_NAME ;
    __unsafe_unretained NSString* PRK_OLD_VALUE ;
    __unsafe_unretained NSString* PRK_NEW_VALUE ;
    __unsafe_unretained NSString* PRK_VALUE_OLDMAP ;
    __unsafe_unretained NSString* PRK_VALUE_NEWMAP ;
    __unsafe_unretained NSString* PRK_QUERY_ST ;
    __unsafe_unretained NSString* PRK_REQUERY_ST ;
    __unsafe_unretained NSString* PRK_SEARCHSPEC_ST ;
    __unsafe_unretained NSString* PRK_REQUERYSEARCHSPEC_ST ;
    __unsafe_unretained NSString* PRK_CUSTOMQUERYEXECUTED_ST ;
    __unsafe_unretained NSString* PRK_INSERT_ST ;
    __unsafe_unretained NSString* PRK_UPDATE_ST ;
    __unsafe_unretained NSString* PRK_DELETE_ST ;
    __unsafe_unretained NSString* PRK_LOG_ST ;
    __unsafe_unretained NSString* LAST_INSERTED_ROWID_QUERY;
    __unsafe_unretained NSString* PRK_RESULT_COUNT_QUERY_FMT;
    __unsafe_unretained NSString* PRK_FROM;
    __unsafe_unretained NSString* PRK_RESULT_COUNT_ALIAS;
    __unsafe_unretained NSString* PRK_DELETESEARCHFIELDVALUES;
    __unsafe_unretained NSString* PRK_UPDATESEARCHFIELDVALUES;
    __unsafe_unretained NSString* PRK_UPDATEFIELDVALUES;
    
    // consts for event ListObject's property names
    __unsafe_unretained NSString* PRK_LISTQUERY_ST ;
    __unsafe_unretained NSString* PRK_LISTSEARCHSPEC_ST ;
    __unsafe_unretained NSString* PRK_LISTREQUERY_ST ;
    __unsafe_unretained NSString* PRK_LISTREQUERYSEARCHSPEC_ST ;
    
    // consts for db types, fields
    
    __unsafe_unretained NSString* K_NULLABLE;
    __unsafe_unretained NSString* K_READONLY;
    __unsafe_unretained NSString* K_LENGTH;
    __unsafe_unretained NSString* V_TRUE;
    __unsafe_unretained NSString* V_FALSE;
    __unsafe_unretained NSString* F_ERRORMESSAGE;
    
    __unsafe_unretained NSString* F_QUALIFIED_JOIN_FIELD;
    __unsafe_unretained NSString* F_QUALIFIED_JOIN_FIELD_C;
    __unsafe_unretained NSString* F_ENCLOSE_IN_APOSTR;
    __unsafe_unretained NSString* F_ENCLOSE_IN_SINGLE_QUOTE;
    
} DaoConsts;


extern const struct Colors {
    __unsafe_unretained NSString* BACKGROUND_COLOR;
    __unsafe_unretained NSString* ERROR_BACK_COLOR;
} Colors;

/* Sub category icon states */
extern const struct SubCategoryStates {
    __unsafe_unretained NSString* BIG_ACTIVE;
    __unsafe_unretained NSString* BIG_CLICKED;
    __unsafe_unretained NSString* BIG;
    __unsafe_unretained NSString* MINI_CLICKED;
    __unsafe_unretained NSString* MINI;
} SubCategoryStates;