//
//  EnumConstants.h
//  Hiro
//
//  Created by George Vashakidze on 12/11/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//


#import<Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, AlertStyle) {
    DARK = 0,
    LIGHT = 1
};