//
//  Constants.h
//  Hiro
//
//  Created by George Vashakidze on 12/7/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

/****************** MODELS ***************************/

/* This structure contains constants for online objects */
const struct Models Models = {
    
    /* Categories */
    .Categories = {
        .ANIMALS                = @"ANIMALS",
        .STEM                   = @"STEM",
        .PEOPLE                 = @"PEOPLE",
        .PLANET                 = @"PLANET"
    },
    
    /* Model names */
    .Names = {
        .USER                   = @"oauth",
        .CATEGORIES             = @"categories",
        .REGISTER               = @"register",
        .CHECK_USER             = @"checkUser",
        .USER_PARAM             = @"/%@"
    },
    
     /* User */
     .User = {
        .ACCESS_TOKEN           = @"access_token",
        .REFRESH_TOKEN          = @"refresh_token",
        .TOKEN_TYPE             = @"token_type",
        .EXPIRES_IN             = @"expires_in",
        .IDENTITY               = @"identity",
        .USER                   = @"user",
        .ID                     = @"id",
        .CREATED_AT             = @"createdAt",
        .UPDATED_AT             = @"updatedAt",
        .DATE                   = @"date",  //Returns date type of 2015-11-20 17:35:54.000000
        .TIMEZONE_TYPE          = @"timezone_type", //Returns number
        .TIMEZONE               = @"timezone", //UTC ...
        .UTC                    = @"UTC",
        .IS_ACTIVE              = @"isActive",
        .EMAIL                  = @"email",
        .USERNAME               = @"userName",
        .USER_LOGIN             = @"username",
        .NAME                   = @"name", //Name of user
        .DESCRIPTION            = @"description", //Nullable
        .SHOUTOUT               = @"shoutout", //Nullable
        .GENDER                 = @"gender", //Nullable
        .AGE_RANGE              = @"ageRange", //Nullable
        .AVATAR                 = @"avatar", //Nullable
        .IS_CONFIRMED           = @"isConfirmed",
        .LAST_LOGIN_AT          = @"lastLoginAt",
        .TOTAL_HERO_CAUSES      = @"totalHeroCauses",
        .TOTAL_LIKED_CAUSES     = @"totalLikedCauses",
        .TOTAL_LIKED_CHAMPIONS  = @"totalLikedChampions",
        .TOTAL_LIKED_HEROS      = @"totalLikedHeroes",
        .TOTAL_LIKES            = @"totalLikes",
        .TOTAL_SHAERS           = @"totalShares",
        .WEBSITE                = @"website", //Nullable
        .RANK                   = @"rank",
        .CATEGORIES             = @"categories",
        .CATEGORY               = @"category",
        .SLUG                   = @"slug",
        .HUMAR_RIGHTS           = @"human_rights",
        .HASH_TAGS              = @"hashtags",
        .HASH_TAG               = @"hashtag",
        .PARENT_CATEGORY        = @"parentCategory", //Nullable
        .PEOPLE                 = @"people",
        .HEALTH_WELLNESS        = @"health_wellness",
        .COMMUNITY              = @"community",
        .TRANSPORTATION         = @"transportation",
        .STEM                   = @"stem", //Possible slug value in response
        .FARM_ANIMALS           = @"farm_animals",
        .F_A_W_B_S              = @"farmAnimals", //farm animals without bottom slash
        .ANIMAL                 = @"animal",
        .ROLES                  = @"roles",
        .ROLE                   = @"role",
        .DEVELOPER              = @"developer",
        .SOCIAL_ACCOUNTS        = @"socialAccounts",
        .SOCIAL_PAGES           = @"socialPages",
        .GRANT_TYPE             = @"grant_type",
        .CLIENT_CREDENTIALS     = @"client_credentials",
        .PASSWORD               = @"password",
        .CLIENT_ID              = @"client_id",
        .CLIENT_ID_VALUE        = @"90845b5c43644646bf6b1c831b6fd8e2",
        .CLIENT_SECRET          = @"client_secret",
        .CLIENT_SECRET_VALUE    = @"c771786f-1444-4842-b179-e47e24492cc8",
        .BEANER_AUTH_HEADER_KEY = @"Bearer ",
        .BEANER_AUTH_HEADER_REGISTER_KEY = @"0ITCBNn4BcvR1NHhXOS0nh36M23y87gCgFn2LSnL",
        .USER_IS_AVAILABLE      = @"free",
        .RESET_PASSWORD         = @"password_reset",
        .DATA                   = @"data"
    }
};
/****************** MODELS ***************************/

/******************REQUEST CONSTANTS***************************/
/* This structure contains constants to work with HTTP requests */
const struct RequestKeys RequestKeys = {
    .REQUEST_KEY             = @"requestKey",
    .BODY                    = @"body",
    .HEADERS                 = @"headers",
    .AUTHORIZATION           = @"Authorization",
    .URI_PARAMS              = @"URIParams",
};
/******************REQUEST CONSTANTS***************************/

/******************REQUEST METHODS***************************/
/* This structure contains constants to work with request methods */
const struct RequestMethods RequestMethods = {
   .GET                      = @"GET",
   .HEAD                     = @"HEAD",
   .POST                     = @"POST",
   .PUT                      = @"PUT",
   .DELETE                   = @"DELETE",
   .CONNECT                  = @"CONNECT",
   .OPTIONS                  = @"OPTIONS",
   .TRACE                    = @"TRACE"
};
/******************REQUEST METHODS***************************/

/******************ERROR CODES***************************/
/* This structure contains constants to work with error codes */
const struct ErrorCodes ErrorCodes = {
   .ERROR                          = @"Error",
   .ATTENTION                      = @"Attention",
   .USERNAME_OR_PASSWORD_INCORRECT = @"The username or password you entered is incorrect",
   .USERNAME_ALREADY_EXISTS        = @"Please select another username",
   .EMAIL_ALREADY_EXISTS           = @"Please select another email",
   .FILL_OUT_ALL_FIELDS            = @"Please fill out all fields",
   .FILL_OUT_ALL_REQUIRED_FIELDS   = @"Please fill out all fields",
   .YOUR_PROCESS_COULD_NOT_BE_COMPLETED   = @"There was an error while trying to finish your request. Please try again later",
   .COULD_NOT_FETCH_CATEGORIES_FROM_SERVER = @"Could not get categories from server. Please try login again later"
    
};
/******************ERROR CODES***************************/

/******************VIEW HELPERS***************************/
/* This structure contains constants to work with XIB/NIB files */
const struct XIBS XIBS = {
    .ALERTHELPER                          = @"AlertHelper"
};
/******************VIEW HELPERS***************************/

const struct Files Files = {
    
    .SETTINGSFILE           = @"amdcstgs.plist",
    .AMDCOM_RES_BUNDLE      = @"amdcomres.bundle",
    .CDM_SERVER_FILE_NAME   = @"server.cdm",
    .CDM_LOCAL_FILE_NAME    = @"local.cdm",
    .METADATA_DIR_PATH      = @"metadata",
    .UID_FILE_NAME          = @"uidef.json",
    .DB_DIR_PATH            = @"db",
    .DB_FILE_NAME           = @"datadb.sqlite",
    .PATH_ATTACHMENTDIR     = @"attachments",
    .PATH_AMDCTEMPDIR       = @"temp",
    
    .Keys = {
        .KEY_UPLOAD_ATTACHMENT_ID       = @"clientId",
        .KEY_DOWNLOAD_ATTACHMENT_ID     = @"parentId",
        .KEY_ATTACHMENTTYPE             = @"type",
        .KEY_FILENAME                   = @"fileName",
        .KEY_REQUESTPARAMS              = @"requestParams"
    }
};

const struct OperationType OperationType = {
    .INSERT         = @"insert",
    .UPDATE         = @"update",
    .DELETE         = @"delete",
    .SUBSCRIBE      = @"subscribe",
    .UNSUBSCRIBE    = @"unsubscribe",
    .CUSTOM         = @"custom"
};

const struct FileUploadRequestConsts FileUploadRequestConsts  = {
    
    .MIMETYPE_OCTETSTREAM = @"application/octet-stream",
    .MIMETYPE_JSON        = @"application/json; charset=UTF-8",
    .CNTENCODING_BINARY  = @"binary",
    .CNTENCODING_8BIT     = @"8bit",
    .MULTIPART_DATA_STRINGFMT = @"\r\n--%@\r\nContent-Disposition: form-data; name=\"%@\"; \r\nContent-Type: %@\r\nContent-Transfer-Encoding: %@\r\n\r\n",
    .MULTIPART_FNAME_STRINGFMT =
    @"\r\n--%@\r\nContent-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\nContent-Type: %@\r\nContent-Transfer-Encoding: %@\r\n\r\n"
    
};

const struct DbTypeNames DbTypeNames = {
    
    .DECIMAL  = @"decimal",
    .VARCHAR  = @"varchar",
    .DATETIME = @"dateTime",
    .TEXT     = @"text",
    .INTEGER  = @"int",
    .DATA     = @"data",
    .CHARACTER = @"character"
};

const struct DateTypes DateTypes = {
    
    .DATETIME = @"dateTime",
    .DATE     = @"date",
    .TIME  = @"time"
};

const struct SqlAggKeyWords SqlAggKeyWords = {
    
    .AGG_COUNT = @"count",
    .AGG_SUM   = @"sum",
    .AGG_AVG   = @"avg",
    .AGG_MIN   = @"min",
    .AGG_MAX   = @"max"
    
};

const struct SqlGenConsts SqlGenConsts = {
    
    .CL_SELECT   = @"SELECT ",
    .CL_INSERT   = @"INSERT INTO ",
    .CL_UPDATE   = @"UPDATE ",
    .CL_DELETE   = @"DELETE FROM ",
    
    .CL_VALUES   = @" VALUES (",
    .CL_SET      = @" SET ",
    
    .CL_OPENPRN = @"(",
    .CL_CLOSEPRN = @")",
    
    .CL_FROM     = @" FROM ",
    .CL_JOIN     = @" JOIN ",
    .CL_ON       = @" ON ",
    .CL_WHERE    = @" WHERE ",
    .CL_GROUPBY  = @" GROUP BY ",
    .CL_ORDERBY  = @" ORDER BY ",
    .CL_LIMIT    = @" LIMIT ",
    .CL_OFFSET   = @" OFFSET ",
    
    .OP_EQ       = @" = ",
    .OP_LIKE     = @" LIKE ",
    .OP_OR       = @" OR ",
    .OP_AND      = @" AND ",
    .SQL_APOSTR  = @"\'",
    .SQL_STAR    = @"*",
    .SQL_PCT     = @"%",
    .SQL_QMARK   = @"?",
    .SQL_UNDER   = @"_",
    
    //match unescaped * for replacement with %
    .REGEXP_STAR = @"(?<!\\\\)([\\*])",
    //match unescaped ? for replacement with _
    .REGEXP_QMARK = @"(?<!\\\\)([\\?])",
    
    .ESCAPED_STAR = @"\\*",
    .ESCAPED_QMARK = @"\\?",
    
    .SQL_DOT    = @".",
    .SQL_COMA    = @",",
    .SQL_FWDSLASH = @"\\",
    .LIKE = @"LIKE",
    
    
    
    .ALIAS_SEP = @".",
    
};

const struct Errors Errors = {
    
    .EXCEPTION_CLASS_NAME = @"exceptionClass",
    .MESSAGE = @"message",
    .EXCEPTION = @"exceptionObj",
    
};

const struct Successes Successes = {
    
    .SUCCESS = @"Success"
    
};

const struct SqlGen SqlGen = {
    
    .Err=  {
        .illegalInputParams = @"Illegal input parameters !"
    }
};

const struct DaoConsts DaoConsts = {
    .OBJECT = @"object",
    .LAST_ERROR_CODE = @"lastErrCode",
    .LAST_ERROR_MESSAGE = @"lastErrMessage",
    .LOGGING_INTO_CHANGELOG_FAILED = @"loggingIntoChangeLogFailed",
    .CONCURENTUPDATEERRORMSG = @"The record has been updated since it was last read.",
    // name for id
    .NAME_FIELDMAP = @"fields",
    .IDENTIFIER    = @"identifier",
    .ROWID    = @"rowId",
    
    // consts for CRUDs log strings
    .LOG_SELECT   = @"SELECT: ",
    .LOG_INSERT   = @"INSERT INTO ",
    .LOG_UPDATE   = @"UPDATE ",
    .LOG_DELETE   = @"DELETE FROM ",
    
    // consts for event object's property names
    .PRK_FIELD_NAME             = @"name", // fieldName for setFieldValue event
    .PRK_OLD_VALUE              = @"originalValue",
    .PRK_NEW_VALUE              = @"value",
    .PRK_VALUE_OLDMAP           = @"oldValueMAP",    // for setValues event
    .PRK_VALUE_NEWMAP           = @"newValueMAP",
    .PRK_QUERY_ST               = @"queryStr",         // for query  event
    .PRK_SEARCHSPEC_ST          = @"searchSpec",  // ___
    .PRK_REQUERY_ST             = @"requeryqueryStr",         // for requery  event
    .PRK_REQUERYSEARCHSPEC_ST   = @"requerysearchSpec",  // ___
    .PRK_CUSTOMQUERYEXECUTED_ST = @"customQueryExecuted", // for customQueryExecuted event
    .PRK_INSERT_ST              = @"insertStr",         // for insert  event
    .PRK_UPDATE_ST              = @"updateStr",         // for update  event
    .PRK_DELETE_ST              = @"deleteStr",         // for delete  event
    .PRK_LOG_ST                 = @"logStr",         // for logStr
    .PRK_DELETESEARCHFIELDVALUES= @"DeleteSearchFiledValues",
    .PRK_UPDATESEARCHFIELDVALUES= @"UpdateSearchFiledValues",
    .PRK_UPDATEFIELDVALUES      = @"UpdateFiledValues",
    
    .LAST_INSERTED_ROWID_QUERY = @"select last_insert_rowid() as rowid ",
    .PRK_RESULT_COUNT_QUERY_FMT = @"SELECT COUNT(1) as resultCount FROM (%@)",
    .PRK_FROM = @"FROM",
    .PRK_RESULT_COUNT_ALIAS = @"resultCount",
    // consts for event ListObject's property names
    .PRK_LISTQUERY_ST             = @"queryStr",    // for listquery  event
    .PRK_LISTSEARCHSPEC_ST        = @"searchSpec",  // ___
    .PRK_LISTREQUERY_ST           = @"requeryStr",         // for requery  event
    .PRK_LISTREQUERYSEARCHSPEC_ST = @"requerysearchSpec",  // ___
    
    .K_NULLABLE = @"nullable",
    .K_READONLY = @"readOnly",
    .K_LENGTH = @"length",
    .V_TRUE = @"true",
    .V_FALSE = @"false",
    .F_ERRORMESSAGE = @"errorMessage",
    .F_QUALIFIED_JOIN_FIELD = @"`%@`.%@",
    .F_QUALIFIED_JOIN_FIELD_C = @"`%s`.%s",
    .F_ENCLOSE_IN_APOSTR = @"`%@`",
    .F_ENCLOSE_IN_SINGLE_QUOTE = @"'%@'"
    
};

const struct Colors Colors = {
    .BACKGROUND_COLOR = @"#2d3438",
    .ERROR_BACK_COLOR = @"#101417"
};

/* Sub category icon states */
const struct SubCategoryStates SubCategoryStates = {
    .BIG_ACTIVE       = @"_Big_Active",
    .BIG_CLICKED      = @"_Big_Clicked",
    .BIG              = @"_Big",
    .MINI_CLICKED     = @"_Mini_Clicked",
    .MINI             = @"_Mini"
};
