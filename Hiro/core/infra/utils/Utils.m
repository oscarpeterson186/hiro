//
//  utils.m
//  AMDCom
//
//  Created by Zviad Jakhua on 6/19/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import "Utils.h"

//load AMDCom resource bundle if not already loaded and retreive localized string by key
NSString* LocalizedString(NSString* key, NSString* comment) {
    static NSBundle* bundle = nil;
    
    if ( bundle == nil ) {
        NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"lem.bundle"];
        bundle = [NSBundle bundleWithPath:path];
        NSLog(@"Current locale: %@, AMDCom Bundle localizations: %@", [[NSLocale currentLocale] localeIdentifier], [bundle localizations]);
    }
    
    return [bundle localizedStringForKey:key value:key table:@"InfoPlist"];
}

NSString* serializeURIParams (NSDictionary * params) {
    NSString * url = @"";
    for (NSString *key in params) {
        NSString *var = [NSString stringWithFormat: @"%@=%@&", key,params[key]];
        url = [url stringByAppendingString:var];
    }
    return [url substringToIndex:[url length] - 1];
}

NSString* base64String(NSString* str) {
    NSData *theData = [str dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

BOOL containsRegExprStr(NSString * regex, NSString * targetStr) {
    
    // with nsstring comparison method
    NSRange match = [targetStr rangeOfString:regex
                                     options:NSRegularExpressionSearch|NSCaseInsensitiveSearch];
    
    if (match.location == NSNotFound) {
        return false;
    } else {
        return true;
    }
}

NSString* replaceRegexpStr(NSString *regexpStr, NSString *replaceWithStr, NSString *targetStr) {
    NSError *error = NULL;
    NSRegularExpression * regexStr = [NSRegularExpression regularExpressionWithPattern:regexpStr options:NSRegularExpressionCaseInsensitive error:&error] ;
    NSString * replacedStr = [regexStr stringByReplacingMatchesInString:targetStr options:0 range:NSMakeRange(0, [targetStr length]) withTemplate:replaceWithStr];
    return replacedStr;
}

/*********************** Json Utils ************************/


NSString* escapeString4JSON(NSString* src) {
    NSMutableString *s = [NSMutableString stringWithString:src];
    [s replaceOccurrencesOfString:@"\"" withString:@"\\\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"/" withString:@"\\/" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}

NSString* getJSONStr (id jsonObj) {
    
#ifndef DEBUG
    NSJSONWritingOptions options = 0;
#else
    NSJSONWritingOptions options = NSJSONWritingPrettyPrinted;
#endif
    
    if ( jsonObj != nil ) {
        
        if ( ![jsonObj isKindOfClass:[NSString class]] ) {
            @autoreleasepool {
                NSError *error_;
                NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonObj options:options error:&error_];
                return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            }
        } else {
            return jsonObj;
        }
        return jsonObj;
    }
    
    return @"null";
}

inline id getJSONObj (NSString* jsonStr) {
    if ( jsonStr == nil ) {
        return nil;
    }
    NSError *error_;
    id json = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error_] ;
    return json;
}

inline id getJSONObjFromData (NSData* jsonData) {
    @try {
        NSError *error_;
        return [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error_] ;
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    return nil;
}

/*********************** Date utils ************************/

NSString* id2JSONStr(id newValue) {
    NSString* valStr;
    //add quotes around all values except numbers
    if(newValue == nil || newValue == [NSNull null])
        valStr = @"null";
    else if ( [newValue isKindOfClass:[NSNumber class]] )
        valStr = [NSString stringWithFormat:@"%@", newValue];
    else if ( [newValue isKindOfClass:[NSString class]] )
        valStr = [NSString stringWithFormat:@"\"%@\"", escapeString4JSON(newValue) ];
    else
        valStr = [NSString stringWithFormat:@"\"%@\"", newValue];
    
    return valStr;
}

/*********************** Json Utils ************************/

inline NSString* arrTOstr(NSArray *arr) {
    NSString *resStr = nil;
    resStr = [arr componentsJoinedByString:@","];
    return resStr;
}

/*********************** Date utils ************************/
inline NSString* date2MilliSecStr(NSDate* date) {
    return [NSString stringWithFormat:@"%llu", date2MilliSecs(date)];
}

inline long long date2MilliSecs(NSDate* date) {
    return (long long)([date timeIntervalSince1970]*1000);
}

inline NSString* quoteString(NSString* src) {
    if ( src != nil )
        return [NSString stringWithFormat:@"\"%@\"", src];
    else
        return @"\"\"";
}

UIColor* colorFromARGBString(NSString* argbColorStr) {
    
    NSString* noHashString = nil;
    if([argbColorStr hasPrefix:@"#"]) {
        noHashString = [argbColorStr substringFromIndex:1];
    } else {
        noHashString = argbColorStr;
    }
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    
    unsigned int hex=0;
    BOOL hexFound = [scanner scanHexInt:&hex];
    if (hexFound == NO)
        return nil;
    
    return colorFromARGB(hex);
}

UIColor* colorFromARGB(unsigned int argbColor) {
    
    int alpha = (argbColor >> 24) & 0xFF;
    if (alpha == 0) {
        alpha = 1.0;
    }
    int r = (argbColor >> 16) & 0xFF;
    int g = (argbColor >> 8) & 0xFF;
    int b = argbColor & 0xFF;
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:alpha];
}

void launchModal(UIViewController *v, NSString * title, NSString * text, AlertStyle style, BOOL *showBlur){
    Alert * alert = [[Alert alloc] initWithNibName:XIBS.ALERTHELPER bundle:nil];
    [alert setColors:(AlertStyle *) style :title :text :showBlur];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    alert.view.frame = CGRectMake(0,0, screenSize.width, screenSize.height);
    v.modalPresentationStyle = UIModalPresentationCurrentContext;
    alert.providesPresentationContextTransitionStyle = YES;
    alert.definesPresentationContext = YES;
    [alert setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [v.view addSubview:alert.view];
    [v presentViewController:alert animated:NO completion:nil];
}

bool InstallAMDCUncaughtExceptionHandler() {
    InstallUncaughtExceptionHandler();
    NSLog(@"%@", @"AMDCom unhcaught exception handler successfully installed");
    return true;
}

void handleException(NSException* exception, NSString* src) {
    NSMutableString* errorMsgBody = [NSMutableString stringWithFormat:@"Unhandled exception caught.\r\n Reason:\r\n%@", [exception reason]];
    NSArray* callStack = [exception.userInfo objectForKey:UncaughtExceptionHandlerAddressesKey];
    for ( int i = 0; i< [callStack count]; i++ )
    {
        [errorMsgBody appendFormat:@"\r\n%@", callStack[i]];
    }
}

void setTextInputStartPosition(UITextField *field){
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    field.leftView = view;
    field.leftViewMode = UITextFieldViewModeAlways;
}

void setButtonTextUnderlineStyle(UIButton * button){
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:button.titleLabel.text];
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    [button setAttributedTitle:commentString forState:UIControlStateNormal];
}

void setButtonTextUnderlineStyleWithTitle(UIButton * button, NSString *title){
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:title];
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    [button setAttributedTitle:commentString forState:UIControlStateNormal];
}

UIColor * getRandomColor() {
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

UIImageView *getAnimatedLoader(UIViewController *v){
    NSURL *fileURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"loader64.gif" ofType:nil]];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:fileURL]];
    FLAnimatedImageView *imageView = [[FLAnimatedImageView alloc] init];
    imageView.animatedImage = image;
    imageView.frame = CGRectMake(0.0, 0.0, 32.0, 32.0);
    imageView.center = v.view.center;
    return imageView;
}

UIActivityIndicatorView * getLoader(UIViewController *v){
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [v.view addSubview:spinner];
    [spinner setFrame:CGRectMake((v.view.frame.size.width/2)-(spinner.frame.size.width/2), (v.view.frame.size.height/2)-(spinner.frame.size.height/2), spinner.frame.size.width, spinner.frame.size.height)];
    return spinner;
}