//
//  AMDCErrorObj.h
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/14/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! @interface AMDCErrorObj
    @discussion Error representation for the AMDCom library
 */
@interface AMDCErrorObj : NSObject

/*!
 @property errorCode
 */
@property(assign, nonatomic) NSInteger code;
/*!
 @property errorMessage message for the AMDCErrorObj
 */
@property(strong, nonatomic) NSString *message;
/*!
 @property errorMessage message for the AMDCErrorObj
 */
@property(strong, nonatomic) NSString *description;
/*!
 @property childErr AMDCErrorObj child error if it is specified
 */
@property(strong, nonatomic) AMDCErrorObj *childErr;

+(AMDCErrorObj*) error : (NSInteger) c message : (NSString *) msg description : (NSString *) desc;
+(AMDCErrorObj*) error : (NSInteger) c message : (NSString *) msg description : (NSString *) desc andChildError : (AMDCErrorObj *) childError;


@end
