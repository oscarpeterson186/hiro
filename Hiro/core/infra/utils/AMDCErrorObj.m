//
//  AMDCErrorObj.m
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/14/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

#import "AMDCErrorObj.h"

@interface AMDCErrorObj()
 
-(id) initWithParams : (NSInteger) c andMsg : (NSString *)msg andDesc : (NSString *) desc andChildError : (AMDCErrorObj *) childError;

@end

@implementation AMDCErrorObj

@synthesize code, message, description, childErr;

+(AMDCErrorObj*) error : (NSInteger) c message : (NSString *) msg description : (NSString *) desc {
    return [[AMDCErrorObj alloc] initWithParams:c andMsg:msg andDesc:desc andChildError:nil];
}

+(AMDCErrorObj*) error : (NSInteger) c message : (NSString *) msg description : (NSString *) desc andChildError : (AMDCErrorObj *) childError {
    return [[AMDCErrorObj alloc] initWithParams:c andMsg:msg andDesc:desc andChildError:childError];
}

/* Internal instance method for init */
-(id) initWithParams : (NSInteger) c andMsg : (NSString *)msg andDesc : (NSString *) desc andChildError : (AMDCErrorObj *) childError {
    self = [super init];
    if(self!=nil) {
        self.code = c;
        self.message = msg;
        self.description = desc;
        self.childErr = childError;
    }
    
    return self;
}

-(id)getAsJSONString
{
    NSMutableDictionary *jsonNDictionary = [NSMutableDictionary dictionaryWithCapacity:5];
    NSString *key;
    id value;
    
    key = NSStringFromSelector(@selector(code));
    value = @(self.code);
    [jsonNDictionary setObject:value forKey:key];
    
    key = NSStringFromSelector(@selector(message));
    value = self.message ?: [NSNull null];
    [jsonNDictionary setObject:value forKey:key];
    
    key = NSStringFromSelector(@selector(description));
    value = self.description ?: [NSNull null];
    [jsonNDictionary setObject:value forKey:key];

    key = NSStringFromSelector(@selector(childErr));
    value = [self.childErr getAsJSONString];
    value = value ?: [NSNull null];
    [jsonNDictionary setObject:value forKey:key];

    return jsonNDictionary;
}

@end
