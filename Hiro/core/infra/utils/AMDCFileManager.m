//
//  AMDCFileManager.m
//  AMDCom
//
//  Created by Zviad Jakhua on 6/12/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

//Responsible for all file operations on the device
#import "Constants.h"
#import "AMDCDBHelper.h"
#import "AMDCFileManager.h"
#import "NSString+AMDCNSString.h"
#import "Utils.h"

@interface AMDCFileManager() {
    
    NSFileManager *fileManager;
    NSString *documentsDirectory;
    NSString* bundlePath;
    
}

@end

@implementation AMDCFileManager

/** Creates the single instance within the application
 
 @return AMDCFileManager
 */
+(AMDCFileManager *) instance {
    static dispatch_once_t pred;
    static AMDCFileManager* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}

-(id)init {
    
    if(self) {
        self->fileManager = [NSFileManager defaultManager];
        self->documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true) objectAtIndex:0];
        self->bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:Files.AMDCOM_RES_BUNDLE];
        NSLog(@"AMDCFileManager: Documents directory path : %@", self->documentsDirectory);
        NSLog(@"AMDCFileManager: Bundle path = %@", self->bundlePath);
    }
    
    return self;
}

-(BOOL) localFileExists : (NSString*) filePath {
    
    if([filePath length]!=0) {
        
        BOOL isDir;
        NSString *relativePath = [self localFileAbsolutePath:filePath];
        BOOL exists = [fileManager fileExistsAtPath:relativePath isDirectory:&isDir];
        if (exists && !isDir){
            return true;
        }
    }
    
    return false;
}

-(BOOL) packageFileExists : (NSString*) fileName {
    
    if([fileName length]>0) {
        NSString *filePath = [self->bundlePath stringByAppendingPathComponent:fileName];
        return [fileManager fileExistsAtPath:filePath];
    }
    
    return false;
}

-(BOOL) packageFileExists : (NSString*) fileName andBundlePath : (NSString *)bundlePath_ {
    
    if([fileName length]>0) {
        
        NSString* filePath = nil;
        
        if([bundlePath_ length] > 0) {
            filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[bundlePath_ stringByAppendingPathComponent:fileName]];
        } else {
            filePath = [bundlePath stringByAppendingPathComponent:fileName];
        }
        
        return [fileManager fileExistsAtPath:filePath];
        
    }
    
    return false;
}

-(BOOL) installPackageFile: (NSString *)package packageFile:(NSString *)packageFile
            andDestination:(NSString *)path andError: (AMDCErrorObj **) error {
    
    
    if([packageFile length] > 0 && [path length] > 0) {
        
        /* First, we should remove the file */
        if ( [self localFileExists:path] ) {
            if ( [self deleteLocalFile:path andError:error] == false ) {
                NSLog(@"AMDCFileManager: File exists, but failed to remove, check if it is being used by another process, file path: %@", path);
            }
        }
        
        /*
         * Here we need to create directories with subdirectories
         */
        NSString *dirPath;
        NSError *err;
        if ( [path indexOf:'/'] == NSNotFound ) {
            dirPath = [self localFileAbsolutePath:path];
        } else {
            dirPath = [self localFileAbsolutePath:[path substringFromRightFromCharacter:'/']];
            if([fileManager createDirectoryAtPath:dirPath withIntermediateDirectories:true attributes:nil error:&err] == false) {
                NSString *desc      = [NSString stringWithFormat:@"AMDCFileManager, installPackageFromBundle, createDirectoryAtPath: %@", dirPath];
                *error              = [AMDCErrorObj error:AMDC_ERR_DIR_CREATE_FAILED message:[err description] description:desc];
                NSLog(@"AMDCFileManager: Failed to install package file, error: %@", [err description]);
                return false;
            }
            
        }
        
        NSString* bndPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:package];
        
        /*
         * Copying file to destination path
         */
        NSLog(@"to path - %@", [self localFileAbsolutePath:path]);
        if([self->fileManager copyItemAtPath:[bndPath stringByAppendingPathComponent:packageFile] toPath:[self localFileAbsolutePath:path] error:&err] == true) {
            //NSLog(@"AMDCFileManager: Package installed successfully, bundle file path : %@, destination path : %@", packageFile, path);
            return true;
            
        } else {
            NSString *desc      = [NSString stringWithFormat:@"AMDCFileManager, installPackageFromBundle, copyItemAtPath: %@, bundleFile: %@", path, packageFile];
            *error              = [AMDCErrorObj error:AMDC_ERR_FILE_COPY_FAILED message:[err description] description:desc];
            NSLog(@"AMDCFileManager: Failed to install package file, error: %@", [err description]);
            return false;
            
        }
        
    } else {
        NSLog(@"AMDCFileManager: Incorrect input parameters, bundleFile : %@, destination path : %@", packageFile, path);
    }
    
    return false;
    
}

//TODO: Zvi verify
-(BOOL) installPackageFile: (NSString *)packageFile andDestination:(NSString *)path andError: (AMDCErrorObj **) error {
    return [self installPackageFile: Files.AMDCOM_RES_BUNDLE packageFile:packageFile andDestination:path
                           andError: error];
}


-(void) removeFilesFromDir : (NSString *) path andError: (AMDCErrorObj **) error{
    if([path length] > 0) {
        
        NSError *err;
        BOOL ok = [self->fileManager changeCurrentDirectoryPath:[self localFileAbsolutePath:path]];
        
        if( ok==false ) {
            
            NSString *msg       = [NSString stringWithFormat:@"AMDCFileManager: Directory does not exits at path %@ !", path];
            NSString *desc      = [NSString stringWithFormat:@"AMDCFileManager, removeFilesFromDir, self->fileManager changeCurrentDirectoryPath: %@", [self localFileAbsolutePath:path]];
            *error              = [AMDCErrorObj error:AMDC_ERR_DIR_NOT_FOUND message:msg description:desc];
            return;
        }
        
        NSArray *files = [self->fileManager contentsOfDirectoryAtPath:[self->fileManager currentDirectoryPath] error:&err];
        
        for (NSString *file in files) {
            
            BOOL isDir;
            if([self->fileManager fileExistsAtPath:file isDirectory:&isDir] && !isDir) {
                
                if([self->fileManager removeItemAtPath:file error:&err]==false) {
                    
                    NSString * msg = err!=nil ? [err description] : [NSString stringWithFormat:@"AMDCFileManager: Failed to remove file %@ !", file];
                    NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, removeFilesFromDir, self->fileManager removeItemAtPath: %@", file];
                    *error         = [AMDCErrorObj error:AMDC_ERR_FILE_DELETE_FAILED message:msg description:desc];
                    NSLog(@"AMDCFileManager: Failed to remove file %@, error : %@", file, [err description]);
                }
            }
        }
        
    }
}

-(NSArray*) findFiles:(NSString*) path extension:(NSString*)extension {
    
    NSArray *dirContents = nil;
    
    if( [path length] > 0) {
        
        NSError* err = nil;
        dirContents = [self->fileManager contentsOfDirectoryAtPath:[self localFileAbsolutePath:path] error:&err];
        
        if ( [extension length] > 0 )
        {
            NSPredicate *fltr = [NSPredicate predicateWithFormat:@"self ENDSWITH %@", [NSString stringWithFormat:@".%@",extension]];
            dirContents = [dirContents filteredArrayUsingPredicate:fltr];
        }
        
    } else {
        NSLog(@"AMDCFileManager: Incorrect input parameter path : %@", path);
    }
    
    return dirContents;
}

-(BOOL) resetUserData: (AMDCErrorObj **) error {
    NSArray *files = [self->fileManager contentsOfDirectoryAtPath:documentsDirectory error:nil];
    BOOL result = true;
    for (int i = 0; i < files.count; i++) {
        NSError *err;
        BOOL ok = [self->fileManager removeItemAtPath:[self localFileAbsolutePath:files[i]] error:&err];
        if( ok==false ) {
            result = ok;
            NSString * msg = err!=nil ? [err description] : [NSString stringWithFormat:@"AMDCFileManager: Failed to remove file %@ !", [self localFileAbsolutePath:files[i]]];
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, resetUserData, self->fileManager removeItemAtPath: %@", [self localFileAbsolutePath:files[i]]];
            *error         = [AMDCErrorObj error:AMDC_ERR_FILE_DELETE_FAILED message:msg description:desc];
            NSLog(@"AMDCFileManager: Failed to remove file %@ !", files[i]);
        }
    }
    return result;
}

-(BOOL) deleteLocalFile : (NSString *) path andError: (AMDCErrorObj **) error {
    if([path length] > 0) {
        NSError *err;
        NSString *path_ = [self localFileAbsolutePath:path];
        if([fileManager removeItemAtPath:path_ error:&err] == true) {
            NSLog(@"AMDCFileManager: Local file removed : %@", [self localFileAbsolutePath:path]);
            return true;
        } else {
            NSString * msg = err!=nil ? [err description] : [NSString stringWithFormat:@"AMDCFileManager: Failed to remove file %@ !", path_];;
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, deleteLocalFile, self->fileManager removeItemAtPath: %@", path_];
            *error         = [AMDCErrorObj error:AMDC_ERR_FILE_DELETE_FAILED message:msg description:desc];
            NSLog(@"AMDCFileManager: Failed to remove file %@, error : %@", path, [err description]);
        }
    }
    NSLog(@"%@", @"AMDCFileManager: Parameter 'path' can not be null");
    return false;
}

-(BOOL) readLocalFile : (NSString *) fileName andData : (NSData **) data andError: (AMDCErrorObj **) error {
    if([fileName length] > 0) {
        if([self localFileExists:fileName] == true) {
            *data = [self->fileManager contentsAtPath:[self localFileAbsolutePath:fileName]];
            return true;
        } else {
            NSString *msg  = [NSString stringWithFormat:@"AMDCFileManager: File does not exist at path - %@", [self localFileAbsolutePath:fileName]];
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, readLocalFile, self->fileManager contentsAtPath: %@", [self localFileAbsolutePath:fileName]];
            *error         = [AMDCErrorObj error:AMDC_ERR_FILE_NOT_FOUND message:msg description:desc];
            NSLog(@"AMDCFileManager: File does not exist at path - %@", [self localFileAbsolutePath:fileName]);
            return false;
        }
        
    } else {
        NSLog(@"%@", @"AMDCFileManager: Parameter 'fileName' can not be null");
        return false;
    }
}

-(BOOL) writeToLocalFile : (NSString *) fileName andData : (NSData *) data overWrite : (BOOL) overWrite andError: (AMDCErrorObj **) error {
    if([fileName length] > 0 && data!=nil) {
        NSUInteger bytesLength = [data length];
        if(bytesLength>=[self freeDiskspaceInBytes]) {
            NSString *msg  = @"AMDCFileManager: There is no free space on the disk";
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, writeToLocalFile, self freeDiskspaceInBytes: %lu", (unsigned long)bytesLength];
            *error = [AMDCErrorObj error:AMDC_ERR_NO_FREE_SPACE message:msg description:desc];
            return false;
        }
        if(overWrite==false) {
            NSFileHandle *handle = [NSFileHandle fileHandleForUpdatingAtPath:[self localFileAbsolutePath:fileName]];
            [handle seekToEndOfFile];
            [handle writeData:data];
            [handle closeFile];
            return true;
        } else {
            if([self localFileExists:fileName]==true) {
                if([self deleteLocalFile:fileName andError:error] == true) {
                    [fileManager createFileAtPath:[self localFileAbsolutePath:fileName] contents:data attributes:nil];
                } else {
                    NSLog(@"AMDCFileManager: Failed to remove file %@ !", fileName);
                    return false;
                }
            } else {
                NSLog(@"AMDCFileManager: Creating local file -> %@", fileName);
                /*
                 * Here we need to create directories with subdirectories
                 */
                NSString *dirPath;
                NSError *err;
                if([fileName indexOf:'/'] == NSNotFound) {
                    dirPath = [self localFileAbsolutePath:fileName];
                } else {
                    dirPath = [self localFileAbsolutePath:[fileName substringFromRightFromCharacter:'/']];
                    if([self->fileManager createDirectoryAtPath:dirPath withIntermediateDirectories:true attributes:nil error:&err] == false) {
                        NSString * msg = err!=nil ? [err description] : [NSString stringWithFormat:@"AMDCFileManager: Failed to create directory: %@ !", dirPath];
                        NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, writeToLocalFile, self->fileManager createDirectoryAtPath: %@", dirPath];
                        *error         = [AMDCErrorObj error:AMDC_ERR_DIR_CREATE_FAILED message:msg description:desc];
                        NSLog(@"AMDCFileManager: Failed to install package file, error: %@", [err description]);
                        return false;
                    }
                }
                /* Creating file to destination path */
                dirPath = [self localFileAbsolutePath:fileName];
                BOOL success = [self->fileManager createFileAtPath:dirPath contents:data attributes:nil];
                if ( success == false ) {
                    NSLog(@"AMDCFileManager: Failed to create file at path: %@/%@", dirPath, fileName);
                }
                return success;
            }
            return true;
        }
    }
    NSLog(@"AMDCFileManager: Incorrect input parameters, fileName: %@, data : %@", fileName, data);
    return false;
    
}

-(BOOL) mkDir : (NSString *) dirPath andError: (AMDCErrorObj **) error {
    if([dirPath length] != 0 && ![dirPath isEqualToString:@"/"]) {
        NSError *err;
        if([self->fileManager createDirectoryAtPath:[self localFileAbsolutePath:dirPath] withIntermediateDirectories:true attributes:nil error:&err] != false) {
            return true;
        } else {
            NSString * msg = err!=nil ? [err description] : [NSString stringWithFormat:@"AMDCFileManager: Failed to create directory: %@ !", dirPath];
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, mkDir, self->fileManager createDirectoryAtPath: %@", dirPath];
            *error         = [AMDCErrorObj error:AMDC_ERR_DIR_CREATE_FAILED message:msg description:desc];
        }
    }
    NSLog(@"%@", @"AMDCFileManager: Incorrect input parameters, dirPath should not be empty");
    return false;
}

-(NSString *) localFileAbsolutePath : (NSString *)relativePath {
    return [self->documentsDirectory stringByAppendingPathComponent:relativePath];
}

-(unsigned long long)freeDiskspaceInBytes
{
    uint64_t totalSpace = 0;
    uint64_t totalFreeSpace = 0;
    
    NSError *error = nil;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, true);
    NSDictionary *dictionary = [self->fileManager attributesOfFileSystemForPath:[paths lastObject] error: &error];
    
    if (dictionary!=nil) {
        NSNumber *fileSystemSizeInBytes = [dictionary objectForKey: NSFileSystemSize];
        NSNumber *freeFileSystemSizeInBytes = [dictionary objectForKey:NSFileSystemFreeSize];
        totalSpace = [fileSystemSizeInBytes unsignedLongLongValue];
        totalFreeSpace = [freeFileSystemSizeInBytes unsignedLongLongValue];
        NSLog(@"AMDCFileManager: Current total space - %llu MB, Free space - %llu MB", ((totalSpace/1024ll)/1024ll), ((totalFreeSpace/1024ll)/1024ll));
    } else {
        NSLog(@"AMDCFileManager: Error Obtaining System Memory Info: Domain = %@, Code = %ld", [error domain], (long)[error code]);
    }
    
    return totalFreeSpace;
}

-(unsigned long long) getFileSize:(NSString*) filePath andError: (AMDCErrorObj **) error {
    NSError* err = nil;
    NSDictionary *fileAttributes = nil;
    unsigned long long result = 0;
    if([filePath length] > 0) {
        fileAttributes = [self->fileManager attributesOfItemAtPath:[self localFileAbsolutePath:filePath] error:&err];
        if ( err != nil ) {
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, getFileSize, self->fileManager attributesOfItemAtPath: %@", [self localFileAbsolutePath:filePath]];
            *error         = [AMDCErrorObj error:AMDC_ERR_FILE_ATTR_READ_FAILED message:[err description] description:desc];
        } else {
            result = [[fileAttributes objectForKey:NSFileSize] unsignedLongLongValue];
        }
    }
    return result;
}

-(BOOL) downloadAttachment:(NSDictionary*)attachmentDescriptor {
    return true;
}

-(uint) getMaxAttachmentSize {
    return 5 * 1024 * 1024;
}

-(BOOL) uploadAttachment:(NSDictionary*)attachmentDescriptor andError: (AMDCErrorObj **) error {
    NSString* fileName = [Files.PATH_ATTACHMENTDIR stringByAppendingPathComponent:[attachmentDescriptor objectForKey:Files.Keys.KEY_FILENAME]];
    if ( [self localFileExists:fileName] ) {
        long long fSize = [self getFileSize:fileName andError:error];
        if (  fSize < [self getMaxAttachmentSize] )
        {
            //TODO Do upload here !!!
            return true;
        }
        else {
            NSString *msg  = [NSString stringWithFormat:LocalizedString(@"File.Err.ExceedAttachLimitSize",nil), fileName];
            NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, uploadAttachment, fSize: %llu", fSize];
            *error         = [AMDCErrorObj error:AMDC_ERR_ATTCHM_SIZE_LIMIT_EXCEED message:msg description:desc];
            NSLog(@"AMDCFileManager: File size exceeds limit for attachment  at path - %@", fileName );
            return false;
        }
    }
    NSLog(@"AMDCFileManager: File does not exist at path - %@", fileName );
    return false;
}

- (unsigned long long)folderSize:(NSString *)folderPath andError: (AMDCErrorObj **) error {
    
    unsigned long long fileSize = 0;
    
    if([folderPath length] > 0) {
        
        NSString *fullPath = [self localFileAbsolutePath:folderPath];
        NSArray *filesArray = [self->fileManager subpathsOfDirectoryAtPath:fullPath error:nil];
        
        NSError *err = nil;
        for(NSString *fileName in filesArray) {
            err = nil;
            NSDictionary *fileDictionary = [self->fileManager attributesOfItemAtPath:[fullPath stringByAppendingPathComponent:fileName] error:&err];
            if (err == nil) {
                fileSize += [fileDictionary fileSize];
            }else{
                
                NSString *desc = [NSString stringWithFormat:@"AMDCFileManager, folderSize, self->fileManager attributesOfItemAtPath: %@", [fullPath stringByAppendingPathComponent:fileName]];
                *error = [AMDCErrorObj error:AMDC_ERR_FILE_ATTR_READ_FAILED message:[err description] description:desc];
                return 0;
            }
        }
        
    }
    
    return fileSize;
}

@end

