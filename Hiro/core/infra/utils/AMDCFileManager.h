//
//  AMDCFileManager.h
//  AMDCom
//
//  Created by Irakli Vashakidze on 6/12/13.
//  Copyright (c) 2013 aMindSolutions. All rights reserved.
//

/*!
 @header AMDCFileManager
 @discussion File manager
 */

#import "AMDCErrorObj.h"

/*!
 
 @interface AMDCFileManager
 Utility class providing the complete functionality for manipulating on application documents directory files.
 
 @updated 2014-05-30
 
 */
@interface AMDCFileManager : NSObject 

/** Public API declaration Start **/

/*!
 * @function instance
 * @discussion Creates instance of File manager.
 * @return Instance of the AMDCFileManager class
 */
+(AMDCFileManager *) instance;

/*!
 * @function localFileExists
 * @discussion Checks if file exists at specified path in application's documents directory
 * @param File relative path
 * @return Boolean value indicating file exists or not.
 */
-(BOOL) localFileExists : (NSString*) filePath;

/*!
 * @function packageFileExists
 * @discussion Checks if file exists in application's default bundle
 * @param File relative path
 * @return Boolean value indicating file exists or not.
 */
-(BOOL) packageFileExists : (NSString*) fileName;

/*!
 * @function packageFileExists
 * @discussion Checks if file exists in the application's spcified bundle
 * @param File relative path
 * @param Bundle relative path
 * @return Boolean value indicating file exists or not.
 */
-(BOOL) packageFileExists : (NSString*) fileName andBundlePath : (NSString *)bundlePath_;

/*!
 * @function findFiles
 * @discussion Returns array of file names in the given specified directory.
 * If extension is specified, files are filtered by extension criteria.
 * @param Directory Path
 * @param extension
 * @return Array of file names.
 */
-(NSArray*) findFiles:(NSString*) path extension:(NSString*)extension;

/*!
 * @function installPackageFileFromBundle
 * @discussion Copies the file from application's default bundle to documents directory.
 * If the file with the same name already exists, it will be removed.
 * If directory path is specified, checks if directory structure exists and creates if any of them are missing.
 * If the file needs to be created in root of the documents directory, then path should contain only file name, either '/path1/path2/fileName.*'.
 * [AMDCEvents.PACKAGE_INSTALLED_SUCCESSFULLY] event is fired, if file was installed successfully.
 * [AMDCEvents.DIR_CREATE_FAILED] event is fired, if directory creation failed.
 * [AMDCEvents.COPY_ITEM_FAILED] event is fired, if file copy failed.
 * @param packageFile
 * @param Destination path
 * @param Error object
 * @return Boolean value indicating the installation result.
 */
-(BOOL) installPackageFile: (NSString *)packageFile andDestination:(NSString *)path andError: (AMDCErrorObj **) error;


/*!
 * @function installPackageFileFromBundle
 * @discussion Copies the file from application's specified bundle to documents directory.
 * If the file with the same name already exists, it will be removed.
 * If directory path is specified, checks if directory structure exists and creates if any of them are missing.
 * If the file needs to be created in root of the documents directory, then path should contain only file name, either '/path1/path2/fileName.*'.
 * [AMDCEvents.PACKAGE_INSTALLED_SUCCESSFULLY] event is fired, if file was installed successfully.
 * [AMDCEvents.DIR_CREATE_FAILED] event is fired, if directory creation failed.
 * [AMDCEvents.COPY_ITEM_FAILED] event is fired, if file copy failed.
 * @param package - Bundle name
 * @param packageFile - Actual file name
 * @param Destination path
 * @param Error object
 * @return Boolean value indicating the installation result.
 */
-(BOOL) installPackageFile: (NSString *)package packageFile:(NSString *)packageFile andDestination:(NSString *)path andError: (AMDCErrorObj **) error;


/*!
 * @function resetUserData
 * @discussion Wipes documents directory
 @return Boolean value indicating whether wipe was successful.
 */
-(BOOL) resetUserData: (AMDCErrorObj **) error;

/*!
 * @function getFileSize
 * @discussion Returns the specified file size in bytes
 * @param Error object
 * @return File size in bytes.
 */
-(unsigned long long) getFileSize:(NSString*) filePath andError: (AMDCErrorObj **) error;

/*!
 * @function deleteLocalfile
 * @discussion Removes the specified file from documents directory
 * @param File path
 * @param Error object
 * @return Boolean value indicating whether delete was successful.
 */
-(BOOL) deleteLocalFile : (NSString *) path andError: (AMDCErrorObj **) error;

/*!
 * @function removeFilesFromDir
 * @discussion Removes all the files from specified directory
 * @param Relative path to directory
 * @param Error object
 * @return void.
 */
-(void) removeFilesFromDir : (NSString *) path andError: (AMDCErrorObj **) error;

/*!
 * @function readLocalFile
 * @discussion Checks if file exists in the doc dir, reads the file and fills the data parameter with file content,
 * If file does not exist at path, [AMDCEvents.FILE_NOT_FOUND] event is fired
 * with the error object as event parameter and NO is returned.
 * @param fileName
 * @param data
 * @param Error object
 * @return Boolean value indicating whether file read was successful.
 */
-(BOOL) readLocalFile : (NSString *) fileName andData : (NSData **) data andError: (AMDCErrorObj **) error;

/*!
 * @function writeToLocalFile
 * @discussion Checks if file exists in the documents directory and if exists either appends
 * specified data parameter or overwrites the whole file content based on passed overWrite parameter.
 * [AMDCEvents.NO_FREE_SPACE] event is fired if the file size exceeds the free disk space.
 * [AMDCEvents.DIR_CREATE_FAILED] event is fired, if file name contains directory path, which creation failed.
 * @param fileName
 * @param data
 * @param overWrite
 * @param Error object
 * @return Boolean value indicating whether file read was successful.
 */
-(BOOL) writeToLocalFile : (NSString *) fileName andData : (NSData *) data overWrite : (BOOL) overWrite andError: (AMDCErrorObj **) error;

/*!
 * @function mkDir
 * @discussion Creates directory under application documents directory with specified path
 * @param dirPath
 * @param Error object
 * @return Boolean value indicating whether directory created.
 */
-(BOOL) mkDir : (NSString *) dirPath andError: (AMDCErrorObj **) error;

/*!
 * @function freeDiskspaceInBytes
 * @discussion Returns an unsingned long number of available free space of disk in bytes
 * @return Free disc space in bytes
 */
-(unsigned long long)freeDiskspaceInBytes;

/*!
 * @function localFileAbsolutePath
 * @discussion Returns local file absolute path (path to documents directory / passed file path)
 * @param Relative path of the file
 * @return File absolute path
 */
-(NSString *) localFileAbsolutePath : (NSString *)relativePath;

/*!
 * @function downloadAttachment
 * @discussion Schedules a task for attachment download
 * @param attachmentDescriptor - map with three mandatory paramaeters, where:
 * KEY_ATTACHEMENT_ID has the value of the AMDCRowId or serverId of the attachment record in the database table
 * KEY_ATTACHMENTTYPE attachment record type, which is the same as the table name e.g. AccountAttachment
 * KEY_FILENAME original file name of the attachemnt file, e.g. mypresentation.ppt , after adding an attachment files
 * are renamed using type_amdcrowid.ext format, where ext is the original file extension. When uploading file name
 * is set to the original file name. Local access to attachemtn files is ALWAYS done via type_amdcrowid.ext format.
 * @return Boolean value indicating whether download task was scheduled successfully.
 */
-(BOOL) downloadAttachment:(NSDictionary*)attachmentDescriptor;

/*!
 * @function uploadAttachment
 * @discussion Schedules a task for attachment upload
 * @param attachmentDescriptor - map with three mandatory paramaeters, where:
 * KEY_ATTACHEMENT_ID has the value of the AMDCRowId or serverId of the attachment record in the database table
 * KEY_ATTACHMENTTYPE attachment record type, which is the same as the table name e.g. AccountAttachment
 * KEY_FILENAME original file name of the attachemnt file, e.g. mypresentation.ppt , after adding an attachment files
 * are renamed using type_amdcrowid.ext format, where ext is the original file extension. When uploading file name
 * is set to the original file name. Local access to attachemtn files is ALWAYS done via type_amdcrowid.ext format.
 * @param Error object
 * @return Boolean value indicating whether upload task was scheduled successfully.
 */
-(BOOL) uploadAttachment:(NSDictionary*)attachmentDescriptor andError: (AMDCErrorObj **) error;

/*!
 * @function folderSize
 * @discussion Returns the directory size in bytes
 * @param Directory path
 * @param Error object
 * @return Directory size in bytes
 */
- (unsigned long long)folderSize:(NSString *)folderPath andError: (AMDCErrorObj **) error;

/** Public API declaration End **/

@end
