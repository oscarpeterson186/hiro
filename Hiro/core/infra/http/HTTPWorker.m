//
//  HTTPWorker.m
//  Hiro
//
//  Created by George Vashakidze on 12/9/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import "HTTPWorker.h"
#import "LemService.h"

@implementation HTTPWorker

+(HTTPWorker *) instance {
    
//    static dispatch_once_t pred;
//    static HTTPWorker* _instance;
//    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });

    HTTPWorker* _instance = [[HTTPWorker alloc] init];
    return _instance;
}

- (void)login:(NSString *) username andPassword:(NSString *) password
{
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  params = [[NSMutableDictionary alloc] initWithCapacity:5];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:2];
    
    [params setObject: Models.User.PASSWORD forKey: Models.User.GRANT_TYPE];
    [params setObject: Models.User.CLIENT_ID_VALUE forKey: Models.User.CLIENT_ID];
    [params setObject: Models.User.CLIENT_SECRET_VALUE forKey: Models.User.CLIENT_SECRET];
    [params setObject: username forKey: Models.User.USER_LOGIN];
    [params setObject: password forKey: Models.User.PASSWORD];
    
    NSString * url = serializeURIParams(params);
    
    [attributes setObject:url forKey:RequestKeys.BODY];
    [attributes setObject:Models.Names.USER forKey:RequestKeys.REQUEST_KEY];
    
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        if(error!=nil || objects == nil) {
            NSLog(@"Error: %@", error.description);
            [_eventManager didLoginSucceed :nil];
        } else {
            [_eventManager didLoginSucceed :objects];
        }
    }];
}

-(void) registerUser :(NSDictionary*) userData :(NSString *)authToken {
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:5];
    NSString * url = serializeURIParams(userData);
    [attributes setObject:url forKey:RequestKeys.BODY];
    [attributes setObject:@{ RequestKeys.AUTHORIZATION : authToken } forKey:RequestKeys.HEADERS];
    [attributes setObject:Models.Names.REGISTER forKey:RequestKeys.REQUEST_KEY];
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        if(error!=nil || objects == nil) {
            if(error == nil){
                [_eventManager didRegisterSucceed :YES];
            }else {
                NSLog(@"Error: %@", error.description);
                [_eventManager didRegisterSucceed :NO];
            }
        } else {
            [_eventManager didRegisterSucceed :YES];
        }
    }];
}

- (void)getAccessTokenForRegister:(NSDictionary*) data
{
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:2];
    NSString * url = serializeURIParams(data);
    [attributes setObject:url forKey:RequestKeys.BODY];
    [attributes setObject:Models.Names.USER forKey:RequestKeys.REQUEST_KEY];
    
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        [_eventManager didGetAccessTokenForRegisterSucceed :objects];
    }];
}

-(void) checkIfUserExists:(NSString*) userName :(NSString*) accessToken
{
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:4];
    NSString * authToken = [NSString stringWithFormat: @"%@%@", Models.User.BEANER_AUTH_HEADER_KEY, accessToken];
    [attributes setObject:@{ RequestKeys.AUTHORIZATION : authToken } forKey:RequestKeys.HEADERS];
    [attributes setObject:@"" forKey:RequestKeys.BODY];
    [attributes setObject:Models.Names.CHECK_USER forKey:RequestKeys.REQUEST_KEY];
    [attributes setObject:[NSString stringWithFormat:Models.Names.USER_PARAM,userName] forKey:RequestKeys.URI_PARAMS];
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        [_eventManager didCheckIfUserExistsSucceed:objects];
    }];
}

-(void) resetPassword:(NSString*) email :(NSString*) accessToken
{
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:4];
    NSString * authToken = [NSString stringWithFormat: @"%@%@", Models.User.BEANER_AUTH_HEADER_KEY, accessToken];
    [attributes setObject:@{ RequestKeys.AUTHORIZATION : authToken } forKey:RequestKeys.HEADERS];
    [attributes setObject:@"" forKey:RequestKeys.BODY];
    [attributes setObject:Models.Names.CHECK_USER forKey:RequestKeys.REQUEST_KEY];
    NSString * resetPassword = [NSString stringWithFormat:Models.Names.USER_PARAM,email];
    [attributes setObject:[NSString stringWithFormat:resetPassword,Models.User.RESET_PASSWORD] forKey:RequestKeys.URI_PARAMS];
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        [_eventManager didPasswordResetSucceed:objects];
    }];
}

-(void) listOfcategories:(NSString *) accessToken
{
    LemService* service = [[LemService alloc] init];
    NSMutableDictionary*  attributes = [[NSMutableDictionary alloc] initWithCapacity:3];
    NSString * authToken = [NSString stringWithFormat: @"%@%@", Models.User.BEANER_AUTH_HEADER_KEY, accessToken];
    [attributes setObject:@"" forKey:RequestKeys.BODY];
    [attributes setObject:Models.Names.CATEGORIES forKey:RequestKeys.REQUEST_KEY];
    [attributes setObject:@{ RequestKeys.AUTHORIZATION : authToken } forKey:RequestKeys.HEADERS];
    [service getObjects: attributes :^(NSDictionary* objects, NSError* error){
        
        //TODO throw event once this method is completed...
        
        if(error!=nil) {
            NSLog(@"Error: %@", error.description);
        } else {
            [_eventManager didGettingCategoriesSucceed:objects];
        }
    }];
}

@end
