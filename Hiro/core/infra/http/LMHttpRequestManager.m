//
//  LMHttpRequestManager.m
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import "LMHttpRequestManager.h"
#import "utils.h"
#import "Constants.h"

static NSString* const HTTPAPI_PLIST_FILE = @"httpapi";
static NSString* const DEFAULT_CONFIG     = @"subscriptions";
static NSString* const CMD_DEVICE_WIPE    = @"deviceWipe";
static NSString* const HDR_ACCEPT         = @"Accept";
static NSString* const HDR_CONTENTTYPE    = @"Content-Type";
static NSString* const HDR_CONTENTTYPEVAL = @"application/x-www-form-urlencoded";
static NSString* const HDR_ACCEPTENCODING = @"Accept-Encoding";
static NSString* const HDR_ENCODINGTYPES  = @"gzip, deflate";
static NSString* const HDR_VERSION  = @"version";

@implementation LMHttpReqConfig

static NSMutableDictionary* httpAPIDef;
@synthesize bodyTemplate,hasUrlBase,headers,httpMethod,maxRetryAttempts,name,retryPeriod,timeOut,url;

+(id)create:(NSString *)name {

    if ( httpAPIDef == nil )
        [LMHttpReqConfig loadHTTPApiDefs];
    
    id cnfg = [[self alloc] init:name config:httpAPIDef[name]];
    return cnfg;
}

-(id)init:(NSString*)name_ config:(NSDictionary*)config_ {
    
    if (self  = [super init]) {
        
        self.name = name_;
        self.headers = config_[@"headers"];
        self.httpMethod = config_[@"httpMethod"];
        self.url = config_[@"url"];
        self.hasUrlBase = [config_[@"hasUrlBase"] boolValue];
        self.bodyTemplate = config_[@"bodyTemplate"];
        self.timeOut = config_[@"timeOut"];
        self.retryPeriod = config_[@"retryPeriod"];
        self.maxRetryAttempts = config_[@"maxRetryAttempts"];
        
        if (self.timeOut == nil)
            self.timeOut = @10;
        
        if (self.retryPeriod)
            self.retryPeriod = @1;

        if (self.maxRetryAttempts)
            self.maxRetryAttempts = @3;
        
    }
    
    return  self;
}

+(BOOL) loadHTTPApiDefs {
    
    NSString* path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"lem.bundle"];
    NSBundle *thisBundle = [NSBundle bundleWithPath:path];
    NSString *plistPath = [thisBundle pathForResource:@"httpapi" ofType:@"plist"];
    
    if ( [plistPath length] > 0 ) {
        httpAPIDef = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
        return true;
    }
    
    NSLog(@"HTTP API Def not found int he bundle, invalid build!");
    return false;
}


@end

@interface LMReqObj() {
    
}

-(void) create: (NSString *) name;
-(void) setRequestURLStr;

@end

@implementation LMReqObj

@synthesize	reqUrl, receivedData, target, reqConfig, body;

+(id) create : (NSString *) name {
    
    if ( name.length == 0 ) {
        return nil;
    }
    
    LMReqObj *obj = [[self alloc] init];
    if(obj!=nil) {
        [obj create:name];
    }
    
    return obj;
}

-(void) create: (NSString *) name {
    self.reqConfig = [LMHttpReqConfig create:name];
    self.baseURL = [NSString stringWithFormat:@"%@:%@", httpAPIDef[@"BaseURL"], httpAPIDef[@"BaseURLPort"]];
    [self setRequestURLStr];
    self.attemptCount = 0;
    self.requestSubmitted = nil;
    self.requestCompleted = nil;

}

//request config tells us if we need to prepend default url base or not. used for external requests, such as weather underground.
-(void)setRequestURLStr
{
    if ( self.reqConfig.hasUrlBase == false )
        self.reqUrl = self.reqConfig.url;
    else
        self.reqUrl = [NSString stringWithFormat:@"%@%@", self.baseURL, self.reqConfig.url];
}

-(NSMutableURLRequest*)createHTTPRequest {
    
    LMHttpReqConfig* rqCfg = self.reqConfig;
    if ( rqCfg != nil )
    {
        NSURL* pathURL = [NSURL URLWithString:self.reqUrl];
        int timeOutSecs = rqCfg.timeOut != nil ? [rqCfg.timeOut intValue] : 10; // ak shegidzlia setting ebidan aigo timeout
        NSMutableURLRequest *httpRequest = [[NSMutableURLRequest alloc] initWithURL: pathURL
                                                                        cachePolicy: NSURLRequestReloadIgnoringLocalCacheData
                                                                    timeoutInterval: timeOutSecs];
        
        [httpRequest setHTTPMethod: rqCfg.httpMethod];
        
        if ( rqCfg.headers != nil ) {
            
            NSEnumerator* e = [rqCfg.headers keyEnumerator];
            NSString* hdrField;
            while( (hdrField = [e nextObject]) )
            {
                NSString *value = [rqCfg.headers objectForKey:hdrField];
                [httpRequest addValue:value forHTTPHeaderField:hdrField];
                
            }
        }

        //check if we have set Accept header field, if not set json as default
        NSString* accept = [httpRequest valueForHTTPHeaderField:HDR_ACCEPT];
        if ( accept == nil )
            [httpRequest addValue:HDR_CONTENTTYPEVAL forHTTPHeaderField:HDR_ACCEPT];
        
        //add encoding to request compressed content
        accept = [httpRequest valueForHTTPHeaderField:HDR_ACCEPTENCODING];
        if ( accept == nil )
            [httpRequest addValue:HDR_ENCODINGTYPES forHTTPHeaderField:HDR_ACCEPTENCODING];
        
        if ([rqCfg.httpMethod isEqualToString:RequestMethods.POST])
        {
            NSMutableData *postBody = [NSMutableData data];
            
            if ( self.body && [self.body length] > 0)
            {
                [postBody appendData:  [self.body dataUsingEncoding:NSUTF8StringEncoding] ];
            }
            
            //default content type to json if it wasn't set by headers already
            NSString* ctype = [httpRequest valueForHTTPHeaderField:HDR_CONTENTTYPE];
            if ( ctype == nil )
                [httpRequest addValue:HDR_CONTENTTYPEVAL forHTTPHeaderField:HDR_CONTENTTYPE];
            
            if ([postBody length] > 0)
            {
                NSString *rLen = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
                [httpRequest addValue:rLen forHTTPHeaderField:@"Content-Length"];
         
                
                [httpRequest setHTTPBody:postBody];
            }
        }
        
        return httpRequest;
    }
    
    return nil;
}


@end

@interface LMHttpRequestManager()

@property (nonatomic, strong) NSURLSessionConfiguration *config;
@property (nonatomic, strong) NSURLSession* session;

@end

@implementation LMHttpRequestManager

+(LMHttpRequestManager *) instance {
    static dispatch_once_t pred;
    static LMHttpRequestManager* _instance;
    dispatch_once(&pred, ^{ _instance = [[self alloc] init]; });
    return _instance;
}

-(id) init {
    
    self = [super init];
    if(self!=nil) {
        self->pendingRequests = [[NSMutableDictionary alloc] initWithCapacity:5];
        self.config = [NSURLSessionConfiguration defaultSessionConfiguration];
        self.session = [NSURLSession sessionWithConfiguration:self.config];
    }
    
    return self;
}

-(LMReqObj*)requestForConnection:(NSURLConnection*)connection {
    
    NSValue* connVal = [NSValue valueWithNonretainedObject:connection];
    return [self->pendingRequests objectForKey:connVal];
}

/**
 Encodes string correctly, including & and / unlike Apple's crappy encode
 **/
+(NSString*)urlEncode:(NSString*)src {
    return [src stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
}

-(void)sendAsync:(LMReqObj*) request completion:(void (^)(NSData* data, NSURLResponse* response, NSError* error))completion {
 
    self.session.configuration.HTTPAdditionalHeaders = request.reqConfig.headers;
    NSMutableURLRequest* req = [request createHTTPRequest];
    NSURLSessionDataTask* task = [self.session dataTaskWithRequest:req completionHandler:completion];
    [task resume];
}

@end
