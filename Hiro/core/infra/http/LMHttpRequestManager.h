//
//  LMHttpRequestManager.h
//  LemondoUtils
//
//  Created by Irakli Vashakidze on 12/8/15.
//  Copyright © 2015 lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LMHttpRequestListener <NSObject>

-(void)onCompleted:(id)object;
-(void)onFail:(id)object;

@end

@interface LMHttpReqConfig : NSObject

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * url;
@property (nonatomic, strong) NSString * httpMethod;
@property (nonatomic, strong) NSDictionary * headers;
@property (nonatomic, assign) BOOL hasUrlBase;
@property (nonatomic, strong) NSString * bodyTemplate;
@property (nonatomic, strong) NSNumber * timeOut;
@property (nonatomic, strong) NSNumber * retryPeriod;
@property (nonatomic, strong) NSNumber * maxRetryAttempts;

+(id)create:(NSString*)name;

@end

@interface LMReqObj : NSObject
{
    int expectedContLen;
}

@property (nonatomic, strong) NSString*               baseURL;
@property (nonatomic, strong) NSString*               port;
@property (nonatomic, strong) NSString*               reqUrl;
@property (nonatomic, strong) NSMutableData*          receivedData;
@property (nonatomic, strong) NSObject*               target;
@property (nonatomic, strong) LMHttpReqConfig*        reqConfig;
@property (nonatomic, strong) NSString*               body;
@property (nonatomic, assign) NSInteger               statusCode;
@property (nonatomic, strong) NSString*               statusMsg;
@property (nonatomic, strong) NSDictionary*           requestParameters;
@property (nonatomic, assign) int                     attemptCount;
@property (nonatomic, strong) NSDate*                 requestSubmitted;
@property (nonatomic, strong) NSDate*                 requestCompleted;

+(id) create : (NSString *) name;

-(NSMutableURLRequest*)createHTTPRequest;

@end

@interface LMHttpRequestManager : NSObject

{
    NSString*				baseUrl;
    NSMutableDictionary*	pendingRequests;
    NSMutableDictionary*    requestConfigs;
    int                     defTimeout;
}

+(LMHttpRequestManager *) instance;
-(void)sendAsync:(LMReqObj*) request completion:(void (^)(NSData* data, NSURLResponse* response, NSError* error))completion;

@end
