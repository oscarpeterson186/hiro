//
//  HTTPWorker.h
//  Hiro
//
//  Created by George Vashakidze on 12/9/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Events.h"

@interface HTTPWorker : NSObject

+(HTTPWorker *) instance;
-(void) login:(NSString *) username andPassword:(NSString *) password;
-(void) listOfcategories:(NSString *) accessToken;
-(void) registerUser :(NSDictionary*) userData :(NSString *)authToken;
-(void) getAccessTokenForRegister:(NSDictionary*) data;
-(void) checkIfUserExists:(NSString*) userName :(NSString*) accessToken;
-(void) resetPassword:(NSString*) email :(NSString*) accessToken;
@property (nonatomic, weak) id <Events> eventManager;

@end
