//
//  main.m
//  Hiro
//
//  Created by George Vashakidze on 12/7/15.
//  Copyright © 2015 Lemondo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
